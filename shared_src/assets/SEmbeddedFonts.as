package assets
{
	import flash.text.Font;

	public class SEmbeddedFonts
	{
		
		/*
		[Embed(source="assets/roFonts/FbSpoilerEng-Regular.otf", embedAsCFF="false",mimeType="application/x-font-opentype", fontFamily="FbSpoilerEng-Regular")]
		private static const font1:Class;
		
		[Embed(source="assets/roFonts/FbTypographEng-Regular.otf", embedAsCFF="false",mimeType="application/x-font-opentype", fontFamily="FbTypographEng-Regular")]
		private static const font2:Class;
		
		[Embed(source="assets/roFonts/Nehama.ttf", embedAsCFF="true",mimeType="application/x-font-truetype", fontFamily="Nehama")]
		private static const font3:Class;
		
		[Embed(source="assets/roFonts/FbTipograf-Regular.otf", embedAsCFF="false",mimeType="application/x-font-opentype", fontFamily="FbTipograf-Regular")]
		private static const font4:Class;

		[Embed(source="fonts/FbTipograf-Regular.otf", embedAsCFF="false",mimeType="application/x-font-opentype", fontName="FbTipograf-Regular", fontFamily="FbTipograf-Regular")]
		public static const font5:Class;
		

		*/
		
		[Embed(source="fonts/FbAgas-Regular.otf", embedAsCFF="true",mimeType="application/x-font-opentype", fontName="FbAgas-Regular", fontFamily="FbAgas-Regular")]
		public static const font8:Class;
		
		[Embed(source="fonts/FbAgas-Regular.otf", embedAsCFF="false",mimeType="application/x-font-opentype", fontName="FbAgas-Regular2", fontFamily="FbAgas-Regular")]
		public static const font9:Class;
		
		[Embed(source="fonts/arial.ttf", embedAsCFF="false",mimeType="application/x-font-truetype", fontFamily="arial11")]
		public static const font6:Class;
		
		[Embed(source="fonts/arial.ttf", embedAsCFF="true",mimeType="application/x-font-truetype", fontFamily="arial2")]
		public static const font7:Class;

		static public function registerFonts():void
		{
			Font.registerFont(font6);
			Font.registerFont(font7);
			Font.registerFont(font8);
			Font.registerFont(font9);
		}

	}
	
}