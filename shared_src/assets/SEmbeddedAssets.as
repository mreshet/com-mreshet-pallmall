package assets
{
	public class SEmbeddedAssets
	{
		/**
		 * splash screen stuff.. embedded for performence - fast loading.
		 */
		[Embed(source="../assets/packages/general/BG.png")]
		static public const _bm_BG: 			Class;		
		
		//[Embed(source="../assets/comps/splash/strip.png")]
		//static public const _bm_strip: 	Class;		
		
		//[Embed(source="../assets/comps/splash/logo.png")]
		//static public const _bm_logo: 		Class;	

		public function SEmbeddedAssets()
		{
		}
	}
}