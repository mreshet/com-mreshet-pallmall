package views
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	
	import feathers.layout.HorizontalLayout;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	
	import views.base.ScreenBase;
	
	public class View_WelcomeScreen extends ScreenBase
	{
		private var _animationDone: Boolean			= false;
		
		private var _cigarettes:			FlexImage		= null;
		private var _imgBg:					FlexImage	= null;
		private var _btnNext:				FlexButton	=	null;
		private var _welcomeImg:			FlexImage		= null;
		private var _smoothImg:				FlexImage		= null;
		private var _tasteImg:				FlexImage		= null;
		
		public function View_WelcomeScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		override protected function initialize():void
		{
			
			super.initialize();
			
			_imgBg					=	new FlexImage();
			_imgBg.scaleMode 		= FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	=	100;
			_imgBg.percentWidth		=	100;
			_imgBg.source		=	"general::bg";
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	=	331;
			_cigarettes.width    =	441;
			_cigarettes.source	=	"general::cigarettes";
			_cigarettes.right = 50;
			_cigarettes.bottom = 75;
			
			_welcomeImg							= new FlexImage();
			_welcomeImg.scaleMode 				= FlexImage.SCALEMODE_STRECTH;
			_welcomeImg.source					=	"general::txt_welcome";
			_welcomeImg.percentWidth			= 709.0/1280;
			_welcomeImg.percentHeight			= 253.0/800;
			_welcomeImg.horizontalAlign 		= HorizontalLayout.HORIZONTAL_ALIGN_RIGHT;
			_welcomeImg.rightPercentWidth		= -0.02;
			_welcomeImg.top = 40;
			
			_smoothImg = new FlexImage();
			_smoothImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_smoothImg.source	=	"general::txt_smooth";
			_smoothImg.height	=	249;
			_smoothImg.width    =	655;
			_smoothImg.left = 0;
			_smoothImg.bottomPercentHeight = 0.3;
			
			_tasteImg = new FlexImage();
			_tasteImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_tasteImg.source	=	"general::taste";
			_tasteImg.height	=	181;
			_tasteImg.width    =	386;
			_tasteImg.right = 420;
			_tasteImg.bottom = 130;
			
			_btnNext							=	CompsFactory.newButton(null, null, btnNext_onTriggered, null, null, false, "general::btn_next_up", true,"center") as FlexButton;
			_btnNext.downIcon					=	CompsFactory.newImage("general::btn_next_down");
			
			_btnNext.percentWidth				=	108.0/1280;
			_btnNext.percentHeight				=	57.0/800;
			//_btnNext.horizontalCenter			=	0;
			_btnNext.topPercentHeight 			= 650.0/800;
			_btnNext.leftPercentWidth			= 50 / 1280;
			_btnNext.iconPercentHeight 			= 1;
			_btnNext.relativeCalcHeightParent	=	this;
			_btnNext.relativeCalcWidthParent	=	this;
			
			addChild(_imgBg);
			
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			addChild(_tasteImg);
			addChild(_btnNext);
			addChild(_backBtn);
			addChild(_cigarettes);
			addChild(_welcomeImg);
			addChild(_smoothImg);
			addChild(_internet);
			
		}
		
		private function btnNext_onTriggered():void
		{
			this._appController.playSound("next");
			_appController.pushScreen("sView_GameScreen");
			
		}
	
		override protected function draw():void
		{
			super.draw();
			
			if (!_animationDone)
			{
				_animationDone = true;
				
				//Cigarettes
				var tween3:Tween = new Tween(_cigarettes, 1);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween3.animate("x", cigX);
				tween3.animate("alpha", 1);
				Starling.juggler.add(tween3);
				
				//Taste
				var tween2:Tween = new Tween(_tasteImg, 1.1);
				var tasteX:int = _tasteImg.x;
				_tasteImg.x += _tasteImg.width;
				_tasteImg.alpha = 0;
				tween2.animate("x", tasteX);
				tween2.animate("alpha", 1);
				Starling.juggler.add(tween2);
				
				//Welcome
				var tween1:Tween = new Tween(_welcomeImg, 1.1);
				var welX:int = _welcomeImg.x;
				_welcomeImg.x += _welcomeImg.width;
				_welcomeImg.alpha = 0;
				tween1.animate("x", welX);
				tween1.animate("alpha", 1);
				Starling.juggler.add(tween1);
				
				//Smooth
				var tween:Tween = new Tween(_smoothImg, 1.1);
				var smoothX:int = _smoothImg.x;
				_smoothImg.x -= _smoothImg.width;
				_smoothImg.alpha = 0;
				tween.animate("x", smoothX);
				tween.animate("alpha", 1);
				Starling.juggler.add(tween);
			}
			
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
		
	}
}