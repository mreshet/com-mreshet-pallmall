package views
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import com.mreshet.mrComps.flexComps.flexLabel.FlexLabel;
	import com.mreshet.mrComps.flexComps.flexTextInput.FlexTextInput;
	import com.mreshet.mrComps.flexComps.vGroup.VGroup;
	
	import flash.text.TextFormat;
	
	import feathers.controls.Button;
	import feathers.core.ToggleGroup;
	
	import starling.display.Quad;
	import starling.events.Event;
	
	import views.base.ScreenBase;

	public class View_HomeScreen extends ScreenBase
	{
		private var _btnPencil:		FlexButton	=	null;
		private var _imgBg:			FlexImage	=	null;
		
		private var _btn1:			FlexButton	=	null;
		private var _btn2:			FlexButton	=	null;
		private var _btn3:			FlexButton	=	null;
		private var _vGrp:			VGroup 			= null;
		private var _tg:			ToggleGroup = null;
		private var _quad:			Quad 				= null;
		
		private var _lblTest:		FlexLabel 	= null;
		
		private var _tiTest:		FlexTextInput	=	null;
		
				
    public function View_HomeScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			_btnPencil	=	null;
		}
		
		override protected function initialize():void
		{
			
			super.initialize();

			var tf:TextFormat	=	new TextFormat("FbAgas-Regular2", 22,0x00ff00);
			var tf2:TextFormat	=	new TextFormat("FbAgas-Regular2", 22,0x00ff00);
			var tf3:TextFormat	=	new TextFormat("FbAgas-Regular2", 22,0x00ff00);
			
			_lblTest	=	CompsFactory.newLabel("2112", tf,false,true,"right", true) as FlexLabel;
			_lblTest.percentHeight	=	10;
			
			_quad	=	new Quad(1,1, 0xc1c1c1);
			
			_btnPencil	=	CompsFactory.newButton(null, null,btnPencil_onTriggered, null, null, false,"general::ss1.btn_pencil_up",true,"center") as FlexButton;
			
			_btnPencil.downIcon	=	CompsFactory.newImage("general::ss1.btn_pencil_down");
			
			_btnPencil.percentWidth		=	20;
			_btnPencil.percentHeight	=	50;
			_btnPencil.horizontalCenter	=	0;
			_btnPencil.topPercentHeight = 0.1;
			_btnPencil.iconPercentHeight = 1;
			_btnPencil.relativeCalcHeightParent	=	this;
			_btnPencil.relativeCalcWidthParent	=	this;
			
			_imgBg	=	new FlexImage();
			_imgBg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	=	100;
			_imgBg.percentWidth	=	100;
			_imgBg.source	=	"general::dropShadow";
			
			
			
			// radio bootons
			_btn1	=	CompsFactory.newButton(null, null,null, tf, "111", true,null,true,"right",true) as FlexButton;
			_btn1.upIcon =_btn1.defaultIcon = new Quad(1,1,0xffffff);
			_btn1.downIcon = new Quad(1,1,0x00);
			_btn1.iconPosition = Button.ICON_POSITION_RIGHT;
			_btn1.percentWidth	=	30;
			_btn1.percentHeight	=	10;
			//_btn1.fontPercentHeight	=	1;
			
			_btn2	=	CompsFactory.newButton(null, null,null, tf2, "111", true,null,true,"right",true) as FlexButton;			
			_btn2.iconPosition = Button.ICON_POSITION_RIGHT;
			_btn2.upIcon =_btn2.defaultIcon = new Quad(1,1,0xffffff);
			_btn2.downIcon = new Quad(1,1,0x00);
			_btn2.percentWidth	=	30;
			_btn2.percentHeight	=	10;
			
			_btn3	=	CompsFactory.newButton(null, null,null, tf3, "111", true,null,true,"right",true) as FlexButton;
			_btn3.upIcon =_btn3.defaultIcon = new Quad(1,1,0xffffff);
			_btn3.downIcon = new Quad(1,1,0x00);
			_btn3.iconPosition = Button.ICON_POSITION_RIGHT;
			_btn3.percentWidth	=	30;
			_btn3.percentHeight	=	10;
			
			_tg	=	new ToggleGroup();
			_tg.addItem(_btn1);
			_tg.addItem(_btn2);
			_tg.addItem(_btn3);
			_tg.selectedItem = null;
			_tg.addEventListener(Event.CHANGE, tg_onChange);
			
			_vGrp = new VGroup();
			_vGrp.addChild(_btn1);
			_vGrp.addChild(_btn2);
			_vGrp.addChild(_btn3);
			
			
			_vGrp.horizontalCenter = 0;
			_vGrp.gapPercentHeight = 2;
			_vGrp.top = 220;
			_vGrp.percentWidth =40;
			_vGrp.percentHeight =100;
			
			
			//addChild(_imgBg);
			addChild(_quad);
			addChild(_btnPencil);
			addChild(_vGrp);
			addChild(_lblTest);
			addChild(_backBtn);
			addChild(_internet);
		}
		
		private function btnPencil_onTriggered(event:Event):void
		{
			//_appController.pushScreen("sView_Screen2");
			_appController.pushScreen("sView_AttendantScereen");
		}
		
		private function tg_onChange(event:Event):void
		{
			var group:ToggleGroup = ToggleGroup( event.currentTarget );
			trace( "group.selectedIndex:", group.selectedIndex );
			// TODO Auto Generated method stub	
		}
		
		override protected function draw():void
		{
			super.draw();
			_quad.width = width;
			_quad.height = height;
			
			
		}
				
	    protected override function onBackButton():void
	    {
	      _appController.showExitWarning();
	    }

	}
}