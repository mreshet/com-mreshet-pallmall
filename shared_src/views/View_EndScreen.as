package views
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import com.mreshet.mrComps.popup.PopupWarning;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import app.QuestionierData;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	
	import views.base.ScreenBase;
	
	public class View_EndScreen extends ScreenBase
	{
		private var _imgBg:					FlexImage	= null;
		private var _btnNext:				FlexButton	= null;
		private var _priceImg:				FlexImage	= null;
		private var _filterImg:				FlexImage	= null;
		private var _animationDone: 		Boolean 	= false;
		private var _cigarettes:			FlexImage	= null;
		private var _taste:			FlexImage	= null;
		//private var _ppFields:			PopupWarning 	= null;
		
		public function View_EndScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		override protected function initialize():void
		{
			
			super.initialize();
			
			_imgBg					=	new FlexImage();
			_imgBg.scaleMode 		= FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	=	100;
			_imgBg.percentWidth		=	100;
			_imgBg.source		=	"general::bg";
			
			_priceImg = new FlexImage();
			_priceImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_priceImg.source	=	"general::price24";
			_priceImg.height	=	147;
			_priceImg.width    =	253;
			_priceImg.right = -10;
			_priceImg.top = 50;
			
			_filterImg = new FlexImage();
			_filterImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_filterImg.source	=	"general::filter";
			_filterImg.height	=	199;
			_filterImg.width    =	630;
			_filterImg.left = -10;
			_filterImg.top = 50;
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	=	331;
			_cigarettes.width    =	441;
			_cigarettes.source	=	"general::cigarettes";
			_cigarettes.right = 50;
			_cigarettes.bottom = 75;
			
			/*_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	  =	468;
			_cigarettes.width     =	658;
			_cigarettes.source	  =		"general::cigarettes";
			_cigarettes.right 	  = 150;
			_cigarettes.bottom 	  = 75;*/
			
			_taste
			_taste = new FlexImage();
			_taste.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_taste.height	  =	106;
			_taste.width     =	414;
			_taste.source	  =		"general::taste_heb";
			_taste.right 	  = 680;
			_taste.bottom 	  = 150;
			
			addChild(_imgBg);
			
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			_btnNext							= CompsFactory.newButton(null, null, btnNext_onTriggered, null, null, false, "general::btn_next_up", true,"center") as FlexButton;
			_btnNext.downIcon					= CompsFactory.newImage("general::btn_next_down");
			
			_btnNext.percentWidth				= 108.0/1280;
			_btnNext.percentHeight				= 57.0/800;
			//_btnNext.horizontalCenter			= 0;
			_btnNext.topPercentHeight 			= 650.0/800;
			_btnNext.leftPercentWidth			= 0.01;
			_btnNext.iconPercentHeight 			= 1;
			_btnNext.relativeCalcHeightParent	= this;
			_btnNext.relativeCalcWidthParent	= this;
			
			addChild(_exitBtn);
			addChild(_btnNext);

			addChild(_backBtn);
			addChild(_priceImg);
			addChild(_filterImg);
			addChild(_taste);
			addChild(_cigarettes);
			addChild(_internet);
		}
		
		
		private function btnNext_onTriggered():void
		{
			//if (_isConnected)
			//{
			//	QuestionierData.instance.addRecord();
				this._appController.playSound("next");
				_appController.pushScreen("sView_ParticipantScreen");
			/*}
			else
			{
				_ppFields = null;
				_ppFields											= new PopupWarning();
				_ppFields.onAction									= popup_onAction;
				//_ppFields.visible 									= true;
				
				_ppFields.textYes									= "ok";
				_ppFields.textHeadline								= "טנרטניאל רוביחב האיגש";
				_ppFields.width = 1280 / 2.0;
				_ppFields.height = 800 / 2.0;
				_ppFields.textWarning = "טנרטניאל רבוחמ רישכמהש אדוול שי";
				_ppFields.validate();
				addChild(_ppFields);
			}*/
		}
		
		/*private function popup_onAction(res:Object):void
		{
			// TODO Auto Generated method stub
			_ppFields.removeFromParent(true);
		}*/
		
		private function startTimer():void 
		{
			var myTimer:Timer = new Timer(3000, 1);
			myTimer.addEventListener("timer", timerHandler);
			myTimer.start();
		}
		
		public function timerHandler(event:TimerEvent):void 
		{
			trace("timerHandler: " + event);
			_appController.pushScreen("sView_ParticipantScreen");
		}
		
		override protected function draw():void
		{
			super.draw();
			
			/*if (_ppFields)
			{
				_ppFields.x = (width - _ppFields.width) / 2.0; 
				_ppFields.y = (height - _ppFields.height) / 2.0;
			}*/
			
			if (!_animationDone)
			{
				_animationDone = true;
				
				//Title
				var tween1:Tween = new Tween(_priceImg, 1);
				var priceX:int = _priceImg.x;
				_priceImg.x += _priceImg.width;
				_priceImg.alpha = 0;
				tween1.animate("x", priceX);
				tween1.animate("alpha", 1);
				Starling.juggler.add(tween1);
				
				//Filter
				var tween2:Tween = new Tween(_filterImg, 1);
				var filterX:int = _filterImg.x;
				_filterImg.x -= _filterImg.width;
				_filterImg.alpha = 0;
				tween2.animate("x", filterX);
				tween2.animate("alpha", 1);
				Starling.juggler.add(tween2);
				
				//Cigarettes
				
				_cigarettes.validate();
				var tween3:Tween = new Tween(_cigarettes, 1.2);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween3.animate("x", cigX);
				tween3.animate("alpha", 1);
				Starling.juggler.add(tween3);
				
				//Taste
				_taste.validate();
				var tween4:Tween = new Tween(_taste, 1);
				var tasteX:int = _taste.x;
				_taste.x += _taste.width;
				_taste.alpha = 0;
				tween4.animate("x", tasteX);
				tween4.animate("alpha", 1);
				Starling.juggler.add(tween4);
			}
			
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
	}
}