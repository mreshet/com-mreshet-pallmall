package views
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import com.mreshet.mrComps.flexComps.flexTextInput.FlexTextInput;
	import com.mreshet.mrComps.flexComps.vGroup.VGroup;
	
	import flash.text.TextFormatAlign;
	
	import app.QuestionierData;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	
	import views.base.ScreenBase;
	
	public class View_AttendantScreen extends ScreenBase
	{
		private var _cigarettes:			FlexImage		= null;
		
		private var _imgBg:			FlexImage	=	null;
		private var _btnNext:		FlexButton	=	null;
		
		
		private var _vGrpAll:		VGroup = null;
		
		private var _vGrp1:			VGroup = null;
		private var _imgLb1:		Image = null;
		private var _name:			FlexTextInput	=	null;
		
		private var _vGrp2:			VGroup = null;
		private var _imgLb2:		Image = null;
		private var _city:			FlexTextInput	=	null;
		private var _animationDone: Boolean			= false;
		
		//private var _vGrp3:			VGroup = null;
		//private var _imgLb3:		Image = null;
		//private var _ti3:		FlexTextInput	=	null;
		
		public function View_AttendantScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
		}
		
		override protected function initialize():void
		{
			
			super.initialize();
			
			_imgBg	=	new FlexImage();
			_imgBg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	=	100;
			_imgBg.percentWidth	=	100;
			_imgBg.source	=	"general::bg";
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	=	331;
			_cigarettes.width    =	441;
			_cigarettes.source	=	"general::cigarettes";
			_cigarettes.right = 50;
			_cigarettes.bottom = 75;
			
			var img1:Image = CompsFactory.newImage("general::txa_red");
			_imgLb1 =  CompsFactory.newImage("general::txt_name");
			_name = new FlexTextInput();
			_name.backgroundSkin = img1;
			_name.height = 57;
			_name.width = 214;
			_name.fontPercentHeight = 0.75;
			_name.textAlign = TextFormatAlign.CENTER;
			_name.color = 0xffffff;
			_name.textInitial = " ";
			_name.text = QuestionierData.instance.getHost();
			_vGrp1 = new VGroup();
			_vGrp1.addChild(_imgLb1);
			_vGrp1.addChild(_name);
			_vGrp1.percentWidth = 100;
			_vGrp1.percentHeight = 25;
			
			
			var img2:Image = CompsFactory.newImage("general::txa_blue");
			_imgLb2 =  CompsFactory.newImage("general::txt_city");
			_city = new FlexTextInput();
			_city.backgroundSkin = img2;
			_city.height = 57;
			_city.width = 214;
			_city.fontPercentHeight = 0.75;
			_city.textAlign = TextFormatAlign.CENTER;
			_city.color = 0xffffff;
			_city.textInitial = " ";
			_city.text = QuestionierData.instance.getCity();
			
			_vGrp2 = new VGroup();
			_vGrp2.addChild(_imgLb2);
			_vGrp2.addChild(_city);
			_vGrp2.percentWidth = 100;
			_vGrp2.percentHeight = 25;
			
			/*var img3:Image = CompsFactory.newImage("general::txa_light_blue");
			_imgLb3 =  CompsFactory.newImage("general::txt_date");
			_ti3 = new FlexTextInput();
			_ti3.backgroundSkin = img3;
			_ti3.height = 57;
			_ti3.width = 214;
			_ti3.fontPercentHeight = 0.5;
			_ti3.textAlign = TextFormatAlign.RIGHT;
			_vGrp3 = new VGroup();
			_vGrp3.addChild(_imgLb3);
			_vGrp3.addChild(_ti3);
			_vGrp3.percentWidth = 100;
			_vGrp3.percentHeight = 25;*/
			
			_vGrpAll = new VGroup();
			_vGrpAll.addChild(_vGrp1);
			_vGrpAll.addChild(_vGrp2);
			//_vGrpAll.addChild(_vGrp3);
			
			_vGrpAll.leftPercentWidth = 0.08;
			_vGrpAll.topPercentHeight = 0.05;
			_vGrpAll.gapPercentHeight = 0;
			//_vGrpAll.top = 100;
			_vGrpAll.percentWidth = 30;
			_vGrpAll.height = 700;
			
			_btnNext							=	CompsFactory.newButton(null, null, btnNext_onTriggered, null, null, false, "general::btn_next_up", true,"center") as FlexButton;
			_btnNext.downIcon					=	CompsFactory.newImage("general::btn_next_down");
			
			_btnNext.percentWidth				=	108.0/1280;
			_btnNext.percentHeight				=	57.0/800;
			//_btnNext.horizontalCenter			=	0;
			_btnNext.topPercentHeight 			= 650.0/800;
			_btnNext.leftPercentWidth			= 50 / 1280;
			_btnNext.iconPercentHeight 			= 1;
			_btnNext.relativeCalcHeightParent	=	this;
			_btnNext.relativeCalcWidthParent	=	this;
			
			
			addChild(_imgBg);
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			addChild(_exitBtn);
			addChild(_btnNext);
			addChild(_vGrpAll);
			addChild(_cigarettes);
			addChild(_internet);
			
		}
		
		private function btnNext_onTriggered():void
		{
			
			if (_name.text == "" || _city.text == "" || _name.text == null || _city.text == null)
				return;
			this._appController.playSound("next");
			QuestionierData.instance.updateAttendant(_name.text, _city.text);
			_appController.pushScreen("sView_ParticipantScreen");
		}
		
		override protected function draw():void
		{
			super.draw();
			
			if (!_animationDone)
			{
				_animationDone = true;
				var tween:Tween = new Tween(_vGrpAll, 2, Transitions.EASE_IN_BACK);
				var currX:int = _vGrpAll.x;
				_vGrpAll.x = -_vGrpAll.width;
				_vGrpAll.alpha = 0;
				tween.animate("x", currX);
				tween.animate("alpha", 1);
				tween.onComplete = function():void { trace("tween complete!"); };
				
				var tween3:Tween = new Tween(_cigarettes, 2, Transitions.EASE_IN_BACK);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween3.animate("x", cigX);
				tween3.animate("alpha", 1);
				
				Starling.juggler.add(tween);
				Starling.juggler.add(tween3);
			}
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
		
	}
}