package views.base
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import com.mreshet.mrGfxManager.MrGfxManager;
	
	import flash.desktop.NativeApplication;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import app.AppController;
	import app.ConnectionChecker;
	
	import feathers.controls.Screen;
	import feathers.layout.VerticalLayout;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;

	public class ScreenBase extends Screen
	{
		protected var _appController:	AppController	=	null;
		protected var _id:				String;
		protected var _backBtn:			FlexButton = null;
		protected var _exitBtn:			FlexButton = null;
		protected var _internet:		FlexImage	= null;
		private   var _checker:ConnectionChecker = new ConnectionChecker();
		protected var _isConnected:Boolean 		 = false; 
		private var _cntr: int = 0;
		private var _cntrExit: int = 0;
		
		protected var _mcBlue:MovieClip			= null;
		private   var _pntBlue:Point			= null;
		protected var _mcRed:MovieClip			= null;
		private   var _pntRed:Point			= null;
		protected var _mcWhite2:MovieClip			= null;
		private   var _pntWhite2:Point			= null;
		
		protected var _mcLight:MovieClip			= null;
		protected var _mcLight2:MovieClip			= null;
		
		public function ScreenBase()
		{
			_appController		=	AppController.instance;
      
		      backButtonHandler = onBackButton;
		      menuButtonHandler = onMenuButton;
	
			  var quad:Quad 				= new Quad(250, 250, 0xFFFFFF, true);
			  quad.alpha = 0;
			  _backBtn						= CompsFactory.newButton(quad, quad, onBackBtn, null, null, false, null, true,"right") as FlexButton;
			  _backBtn.width				= 250;
			  _backBtn.height				= 250;
			  _backBtn.rightPercentWidth = 0;
			  _backBtn.bottomPercentHeight = 0;
			  _backBtn.verticalAlign		= VerticalLayout.VERTICAL_ALIGN_BOTTOM;
			  _backBtn.iconPercentHeight 	= 1;
			  
			  var quadExit:Quad 				= new Quad(250, 250, 0xFFFFFF, true);
			  quadExit.alpha = 0;
			  _exitBtn						= CompsFactory.newButton(quadExit, quadExit, onExitBtn, null, null, false, null, true,"right") as FlexButton;
			  _exitBtn.width				= 250;
			  _exitBtn.height				= 250;
			  _exitBtn.leftPercentWidth = 0;
			  _exitBtn.bottomPercentHeight = 0;
			  _exitBtn.verticalAlign		= VerticalLayout.VERTICAL_ALIGN_BOTTOM;
			  _exitBtn.iconPercentHeight 	= 1;
			  
			  _internet = new FlexImage();
			  _internet.scaleMode = FlexImage.SCALEMODE_STRECTH;
			  _internet.height	  =	25;
			  _internet.width     =	25;
			  _internet.right 	  = 2;
			  _internet.top 	  = 2;
			  
			  
			  var mcBv: Vector.<Texture>  = MrGfxManager.instance.getTextures("animation::animationBlue");
			  _mcBlue = new MovieClip(mcBv, 15);
			  _pntBlue = new Point(-140, 253);
			  _mcBlue.x = _pntBlue.x;
			  _mcBlue.y = _pntBlue.y;
			  _mcBlue.loop = false;
			  _mcBlue.width = 867*1.1;
			  _mcBlue.height = 260*1.1;
			  
			  //var mcWv: Vector.<Texture>  = MrGfxManager.instance.getTextures("animation::animationWhite");
			  //_mcWhite = new MovieClip(mcWv, 12);
			  //_mcWhite.x = 400;
			  //_mcWhite.y = 150;
			  //_mcWhite.loop = false;
			  //_mcWhite.width = 1380;
			  //_mcWhite.height = 633;
			  
			  var mcW2v: Vector.<Texture>  = MrGfxManager.instance.getTextures("animation::animationWhite2");
			  _mcWhite2 = new MovieClip(mcW2v, 12);
			  _pntWhite2 = new Point(104, -202);
			  _mcWhite2.x = _pntWhite2.x;
			  _mcWhite2.y = _pntWhite2.y;
			  _mcWhite2.loop = false;
			  _mcWhite2.width = 1380;
			  _mcWhite2.height = 633;
			  
			  var mcLv: Vector.<Texture>  = MrGfxManager.instance.getTextures("animation::lightexplo");
			  _mcLight = new MovieClip(mcLv, 12);
			  _mcLight.x = 629;
			  _mcLight.y = 232;
			  _mcLight.loop = true;
			  _mcLight.width = 207*1.1;
			  _mcLight.height = 174*1.1;
			  Starling.juggler.add(_mcLight);
			  
			  var mcL2v: Vector.<Texture>  = MrGfxManager.instance.getTextures("animation::lightexplo");
			  _mcLight2 = new MovieClip(mcL2v, 16);
			  _mcLight2.x = 975;
			  _mcLight2.y = 252;
			  _mcLight2.loop = true;
			  _mcLight2.width = 142*1.1;
			  _mcLight2.height = 128*1.1;
			  Starling.juggler.add(_mcLight2);
			  
			  var mcRv: Vector.<Texture>  = MrGfxManager.instance.getTextures("animation::animationRed");
			  _mcRed = new MovieClip(mcRv, 12);
			  _pntRed = new Point(-16, 350);
			  _mcRed.x = _pntRed.x;
			  _mcRed.y = _pntRed.y;
			  _mcRed.loop = false;
			  _mcRed.width = 867*1.1;
			  _mcRed.height = 260*1.1;
			  
			  
			  _checker.addEventListener(ConnectionChecker.EVENT_SUCCESS, checker_success);
			  _checker.addEventListener(ConnectionChecker.EVENT_ERROR, checker_error);
			  _checker.check();
			  startInternetTimer();
			  startAnimTimer();
		}
		
		private function startAnimTimer():void
		{
			var myTimer:Timer = new Timer(randomIntBetween(500,1000), int.MAX_VALUE);
			myTimer.addEventListener("timer", timerAnimHandler);
			myTimer.start();
		}
		
		private function randomRange(minNum:Number, maxNum:Number):Number
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		
		protected function timerAnimHandler(event:TimerEvent):void
		{
			var r:int = randomIntBetween(1,3);
			switch(r)
			{
				case 1:
					trace("blue anim");
					_mcBlue.stop();
					Starling.juggler.remove(_mcBlue);
					_mcBlue.x = _pntBlue.x * randomRange(0.9, 1.1);
					_mcBlue.y = _pntBlue.y * randomRange(0.9, 1.1);
					//_mcBlue.rotation = randomRange(-0.05, 0.05);
					Starling.juggler.add(_mcBlue);
					_mcBlue.play();
					break;
				case 2:
					trace("white anim2");
					_mcWhite2.stop();
					Starling.juggler.remove(_mcWhite2);
					_mcWhite2.x = _pntWhite2.x * randomRange(0.9, 1.1);
					_mcWhite2.y = _pntWhite2.y * randomRange(0.9, 1.1);
					//_mcWhite2.rotation = randomRange(-0.05, 0.05);
					Starling.juggler.add(_mcWhite2);
					_mcWhite2.play();
					break;
				case 3:
					trace("red anim");
					_mcRed.stop();
					Starling.juggler.remove(_mcRed);
					_mcRed.x = _pntRed.x * randomRange(0.95, 1.05);
					_mcRed.y = _pntRed.y * randomRange(0.95, 1.05);
					//_mcRed.rotation = randomRange(-0.05, 0.05);
					Starling.juggler.add(_mcRed);
					_mcRed.play();
					break;
				//case 4:
					//trace("light");
					//Starling.juggler.add(_mcLight);
					//_mcLight.play();
					//break;
				default:
				trace("Error anim");
			}
		}
			
		private function randomIntBetween(min:int, max:int):int {
			return Math.round(Math.random() * (max - min) + min);
		}
		

		
		
		private function checker_success(event:Object):void
		{
			_isConnected = true;
			_internet.source	  =		"general::on";
		}
		
		private function checker_error(event:Object):void
		{
			_isConnected = false;
			_internet.source	  =		"general::off";
		}

		
		private function onBackBtn():void
		{
			trace("onBackBtn");
			if (_cntr == 0)
				startTimer();
			_cntr++;
			
			if (_cntr >= 3)
				_appController.pushScreen("sView_AttendantScreen");
		}
		
		private function onExitBtn():void
		{
			trace("onExitBtn");
			if (_cntrExit == 0)
				startExitTimer();
			_cntrExit++;
			
			if (_cntrExit >= 3)
				NativeApplication.nativeApplication.exit();
		}
		
    protected function onMenuButton():void
    {
      //MrSnapShot.takeSnapshot();
    }
    
    /**
     * put implememntation here once i have a screen stack design 
     */
    protected function onBackButton():void
    {
		
    }
	
	private function startInternetTimer():void 
	{
		var myTimer:Timer = new Timer(60000, int.MAX_VALUE);
		myTimer.addEventListener("timer", timerInternetHandler);
		myTimer.start();
	}
	
	protected function timerInternetHandler(event:TimerEvent):void
	{
		_checker.check();
		
	}
	
	private function startTimer():void 
	{
		var myTimer:Timer = new Timer(3000, 1);
		myTimer.addEventListener("timer", timerHandler);
		myTimer.start();
	}
	
	private function startExitTimer():void 
	{
		var myTimer:Timer = new Timer(3000, 1);
		myTimer.addEventListener("timer", timerExitHandler);
		myTimer.start();
	}
	
	private function timerHandler(event:TimerEvent):void 
	{
		_cntr = 0;
	}
	
	private function timerExitHandler(event:TimerEvent):void 
	{
		_cntrExit = 0;
	}
	
    
	protected override function screen_removedFromStageHandler(event:Event):void
		{
      super.screen_removedFromStageHandler(event);
		}

		public function get id():							String	{	return _id;		}
		public function set id(value:String):	void		{	_id = value;	}

		protected override function screen_addedToStageHandler(event:Event):void
		{
      		super.screen_addedToStageHandler(event);
		}
		
	}
}