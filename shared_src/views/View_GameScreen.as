package views
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import com.mreshet.mrComps.flexComps.vGroup.VGroup;
	
	import app.QuestionierData;
	
	import feathers.layout.HorizontalLayout;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	
	import views.base.ScreenBase;
	
	public class View_GameScreen extends ScreenBase
	{
		private var _animationDone: Boolean			= false;
		private var _cigarettes:			FlexImage	= null;
		
		public function View_GameScreen()
		{
			super();
		}
		
		private var _imgBg:				FlexImage	= null;
		private var _title:				FlexImage	= null;
		private var _currentPage:		int	= 5;
		private var _vGrp:				VGroup 		= null;
		private var _btnQ1:				FlexButton	= null;
		private var _btnQ2:				FlexButton	= null;
		private var _btnQ3:				FlexButton	= null;
		private var _initPage:			int = 5; 
		
				
		override public function dispose():void
		{
			super.dispose();
		}
		
		override protected function initialize():void
		{
			
			super.initialize();
			
			if (QuestionierData.instance.getSex() == "Male" && QuestionierData.instance.getPrefer() == "Women")
				_initPage = 5;
			else if (QuestionierData.instance.getSex() == "Female" && QuestionierData.instance.getPrefer() == "Men")
				_initPage = 8;
			else if (QuestionierData.instance.getSex() == "Male" && QuestionierData.instance.getPrefer() == "Men")
				_initPage = 11;
			else if (QuestionierData.instance.getSex() == "Female" && QuestionierData.instance.getPrefer() == "Women")
				_initPage = 14;
			
			_currentPage = _initPage;
			
			_imgBg						= new FlexImage();
			_imgBg.scaleMode 			= FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight		= 100;
			_imgBg.percentWidth			= 100;
			_imgBg.source				= "general::bg";
			
			_title						= new FlexImage();
			_title.scaleMode 			= FlexImage.SCALEMODE_LETTERBOX;
			_title.percentHeight		= 0.32;
			_title.percentWidth			= 1;
			_title.horizontalAlign 		= HorizontalLayout.HORIZONTAL_ALIGN_RIGHT;
			_title.source				= "general::title" + _currentPage.toString();

			_btnQ1						= CompsFactory.newButton(null, null, btnQ_onTriggered1, null, null, false, "general::" + _currentPage.toString() + "_1", true,"center") as FlexButton;
			_btnQ1.percentWidth			= 100;
			_btnQ1.percentHeight		= 26;
			_btnQ1.iconPercentHeight 	= 0.99;
			
			
			_btnQ2						= CompsFactory.newButton(null, null, btnQ_onTriggered2, null, null, false, "general::" + _currentPage.toString() + "_2", true,"center") as FlexButton;
			_btnQ2.percentWidth			= 100;
			_btnQ2.percentHeight		= 26;
			_btnQ2.iconPercentHeight 	= 1;
			

			_btnQ3						= CompsFactory.newButton(null, null, btnQ_onTriggered3, null, null, false, "general::" + _currentPage.toString() + "_3", true,"center") as FlexButton;
			_btnQ3.percentWidth			= 100;
			_btnQ3.percentHeight		= 26;
			_btnQ3.iconPercentHeight 	= 1;
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	=	331;
			_cigarettes.width    =	441;
			_cigarettes.source	=	"general::cigarettes";
			_cigarettes.right = 50;
			_cigarettes.bottom = 75;
			
			
			
			_vGrp = new VGroup();
			_vGrp.addChild(_btnQ1);
			_vGrp.addChild(_btnQ2);
			_vGrp.addChild(_btnQ3);
			_vGrp.leftPercentWidth = 0.07;
			_vGrp.bottomPercentHeight = 0.25;
			_vGrp.gapPercentHeight = 0.05;
			//_vGrpAll.top = 100;
			_vGrp.percentWidth = 0.5;
			_vGrp.percentHeight = 0.4;
					
			addChild(_imgBg);
			
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			addChild(_vGrp);
			addChild(_title);
			addChild(_backBtn);
			addChild(_cigarettes);
			addChild(_internet);
			addChild(_exitBtn);
		}
		
		private function btnQ_onTriggered1():void
		{
			this._appController.playSound("question");
			QuestionierData.instance.addScores(1);
			updateQuestions();
		}
		private function btnQ_onTriggered2():void
		{
			this._appController.playSound("question");
			QuestionierData.instance.addScores(2);
			updateQuestions();
		}
		private function btnQ_onTriggered3():void
		{
			this._appController.playSound("question");
			QuestionierData.instance.addScores(3);
			updateQuestions();
		}
			
		private function updateQuestions():void
		{
			trace("Answer: " + _currentPage);
			
			
			_currentPage++;
			
			if (_currentPage - _initPage > 2)	
			{ 
				_appController.pushScreen("sView_CalcScreen");
			}
			
			if (_currentPage >= 17 || _currentPage - _initPage > 2)
				return;
			
			_title.source				= "general::title" + _currentPage.toString();
			_btnQ1.defaultIcon			= CompsFactory.newImage("general::" + _currentPage.toString() + "_1");
			_btnQ2.defaultIcon			= CompsFactory.newImage("general::" + _currentPage.toString() + "_2");
			_btnQ3.defaultIcon			= CompsFactory.newImage("general::" + _currentPage.toString() + "_3");
			_btnQ1.invalidate();
			_btnQ2.invalidate();
			_btnQ3.invalidate();
			_animationDone = false;
		}		
		
		override protected function draw():void
		{
			super.draw();
			
			if (!_animationDone)
			{
				_animationDone = true;
				//Title
				var tween:Tween = new Tween(_title, 1);
				var titleX:int = _title.x;
				_title.x += _title.width;
				_title.alpha = 0;
				tween.animate("x", titleX);
				tween.animate("alpha", 1);
				Starling.juggler.add(tween);
				
				//Question1
				var tween1:Tween = new Tween(_btnQ1, 0.5);
				_btnQ1.alpha = 0;
				_btnQ1.scaleX = 0.9;
				_btnQ1.scaleY = 0.9;
				tween1.animate("scaleX", 1);
				tween1.animate("scaleY", 1);
				tween1.animate("alpha", 1);
				Starling.juggler.add(tween1);
				
				//Question1
				var tween2:Tween = new Tween(_btnQ2, 0.8);
				_btnQ2.alpha = 0;
				_btnQ2.scaleX = 0.9;
				_btnQ2.scaleY = 0.9;
				tween2.animate("scaleX", 1);
				tween2.animate("scaleY", 1);
				tween2.animate("alpha", 1);
				Starling.juggler.add(tween2);
				
				//Question1
				var tween3:Tween = new Tween(_btnQ3, 1.1);
				_btnQ3.alpha = 0;
				_btnQ3.scaleX = 0.9;
				_btnQ3.scaleY = 0.9;
				tween3.animate("scaleX", 1);
				tween3.animate("scaleY", 1);
				tween3.animate("alpha", 1);
				Starling.juggler.add(tween3);
				
				var tween4:Tween = new Tween(_cigarettes, 1);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween4.animate("x", cigX);
				tween4.animate("alpha", 1);
				Starling.juggler.add(tween4);
				
			}
			
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
	}
}