package views
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import com.mreshet.mrComps.flexComps.flexTextInput.FlexTextInput;
	import com.mreshet.mrComps.flexComps.hGroup.HGroup;
	import com.mreshet.mrComps.flexComps.vGroup.VGroup;
	import com.mreshet.mrComps.popup.PopupWarning;
	import com.mreshet.mrUtils.SColors;
	
	import flash.events.Event;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import app.QuestionierData;
	
	import feathers.controls.Button;
	import feathers.controls.List;
	import feathers.controls.PickerList;
	import feathers.controls.popups.VerticalCenteredPopUpContentManager;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.ToggleGroup;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	
	import views.base.ScreenBase;
	
	public class View_ParticipantScreen extends ScreenBase
	{
		private var _cigarettes:			FlexImage		= null;
		private var _imgBg:					FlexImage		= null;
		private var _btnNext:				FlexButton		= null;
		
		//Mighty God
		private var _vGrpAll:				VGroup 			= null;
		
		//HGroup - Age & Gender
		private var _hGrpAgeGender:			HGroup 			= null;
	
		private var _vGrpAge:				VGroup			= null;
		private var _imgLbAge:				Image 			= null;
		private var _tiAge:					FlexTextInput	= null;
		private var _vGrpGender:			VGroup			= null;
		private var _imgLbGender:			Image 			= null;
		private var _tiGender:				FlexTextInput	= null;
		private var _hGrpGndr:   			HGroup			= null; 
		
		//HGroup - Smoking & Code
		private var _hGrpSmokingCode:		HGroup 			= null;
		
		//HGroup - Age & Gender
		private var _vGrpCodeSmoking:		HGroup 			= null;
		private var _vGrpSmoking:			VGroup			= null;
		private var _imgLbSmoking:			Image 			= null;
		private var _tgGender:				ToggleGroup 	= null;
		private var _btnFemale:				FlexButton 		= null;
		private var _btnMale:				FlexButton 		= null;
		private var _gender:				String			= "";
		
		private var _listSmoking:PickerList = new PickerList();
		private var _tiSmoking:				FlexTextInput	= null;
		
		private var _vGrpCode:				VGroup			= null;
		private var _imgLbCode:				Image 			= null;
		private var _tiCode:				FlexTextInput	= null;
		
		//Email
		private var _vGrpEmail:				VGroup			= null;
		
		private var _imgLbEmail:			Image 			= null;
		private var _tiEmail:				FlexTextInput	= null;
		
		//Terms
		private var _imgLbTerms:			Image 			= null;
		private var _AgreeBtn:				FlexButton		= null;
		
		private var _fontColor:				uint			=	SColors.DARK_BLUE;
		
		private var _isAgree:				Boolean			= false;
		
		private var _ppFields:			PopupWarning 	= null;
		
		private var _errorMsg:			String			= "";
		
		private var _animationDone: Boolean			= false;
		
		public function View_ParticipantScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		override protected function initialize():void
		{
			
			super.initialize();
			
			_errorMsg = "";
			_animationDone = false;

			QuestionierData.instance.clearParticipant();
				
			_imgBg	=	new FlexImage();
			_imgBg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	=	100;
			_imgBg.percentWidth		=	100;
			_imgBg.source	=	"general::bg";
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	=	331;
			_cigarettes.width    =	441;
			_cigarettes.source	=	"general::cigarettes";
			_cigarettes.right = 50;
			_cigarettes.bottom = 75;
			
			addAgeGender();
			addSmokingCode();
			addEmail();
			//Add terms 
			_AgreeBtn = CompsFactory.newButton("general::txt_agree2","general::txt_agree1",onAgreeChange,null,null,true,null,true,"right",false) as FlexButton;
			_AgreeBtn.percentHeight = 12;
			_AgreeBtn.percentWidth = 100;
			_AgreeBtn.right = 0;
			
			_vGrpAll = new VGroup();
			_vGrpAll.addChild(_hGrpAgeGender);
			_vGrpAll.addChild(_hGrpSmokingCode);
			_vGrpAll.addChild(_vGrpEmail);
			_vGrpAll.addChild(_AgreeBtn);
			_vGrpAll.x = -400;
			_vGrpAll.topPercentHeight = 0;
			_vGrpAll.gapPercentHeight = 0;
			//_vGrpAll.top = 100;
			_vGrpAll.width = 400;
			_vGrpAll.height = 700;
			
			_btnNext							= CompsFactory.newButton(null, null, btnNext_onTriggered, null, null, false, "general::btn_next_up", true,"center") as FlexButton;
			_btnNext.downIcon					= CompsFactory.newImage("general::btn_next_down");
			
			_btnNext.percentWidth				= 108.0/1280;
			_btnNext.percentHeight				= 57.0/800;
			//_btnNext.horizontalCenter			= 0;
			_btnNext.topPercentHeight 			= 650.0/800;
			_btnNext.leftPercentWidth			= 50 / 1280;
			_btnNext.iconPercentHeight 			= 1;
			_btnNext.relativeCalcHeightParent	= this;
			_btnNext.relativeCalcWidthParent	= this;
			
			addChild(_imgBg);
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			addChild(_btnNext);
			addChild(_vGrpAll);
			
			
			
			
			addChild(_listSmoking);
			addChild(_hGrpGndr);
			addChild(_backBtn);
			addChild(_cigarettes);
			addChild(_internet);
			
			_listSmoking.visible = true;
			//_listSmoking.validate();
		}
		
		private function popup_onAction(res:Object):void
		{
			// TODO Auto Generated method stub
			_ppFields.removeFromParent(true);
			_errorMsg = "";
		}
		
		private function onAgreeChange():void
		{
			// TODO Auto Generated method stub
			_isAgree = !_isAgree;
			trace("_isAgree = " + _isAgree);
		}
		
		private function isValidUserEmail(email:String):Boolean 
		{
			var myRegExp:RegExp = /^[a-z][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
			var myResult:Object = myRegExp.exec(email);
			if (myResult == null) 
			{
				return false;
			}
			return true;
		}
		
		private function addEmail():void
		{
			var img:Image = CompsFactory.newImage("general::txa_light_blue");
			_imgLbEmail =  CompsFactory.newImage("general::txt_mail");
			_tiEmail = new FlexTextInput();
			_tiEmail.restrict="a-zA-Z0-9_\\-@.";
			_tiEmail.backgroundSkin = img;
			_tiEmail.height = 78;
			_tiEmail.width = 200*2;
			_tiEmail.fontPercentHeight = 0.7;
			_tiEmail.textAlign = TextFormatAlign.CENTER;
			_tiEmail.color = 0xffffff;
			
			_vGrpEmail = new VGroup();
			_vGrpEmail.addChild(_imgLbEmail);
			_vGrpEmail.addChild(_tiEmail);
			_vGrpEmail.percentWidth = 0.45;
			_vGrpEmail.percentHeight = 0.27;
		}
		
		private var trf: Function = function(): TextFieldTextRenderer
		{
			var tfr: TextFieldTextRenderer = new TextFieldTextRenderer();
			
			//tfr.embedFonts 									= true;
			tfr.textFormat 								= new TextFormat("arial11", height*0.06, _fontColor);
			tfr.textFormat.align 						= TextFormatAlign.RIGHT;
			tfr.textFormat.rightMargin = 15;
			tfr.embedFonts								=	true;
			return tfr;
		}
			
		private var trf2: Function = function(): TextFieldTextRenderer
		{
			var tfr: TextFieldTextRenderer = new TextFieldTextRenderer();
			//tfr.embedFonts 									= true;
			tfr.textFormat 									= new TextFormat('FbAgas-Regular', 28, 0xffffff);
			tfr.embedFonts=true;
			
			
			return tfr;
		}
			
		
		private function addSmokingCode():void
		{
			var img1:Image = CompsFactory.newImage("general::txa_red");
			_imgLbSmoking =  CompsFactory.newImage("general::txt_brand");
			
			var groceryList:ListCollection = new ListCollection(
				[
					{ text: "Pall Mall"},
					{ text: "Marlboro Gold"},
					{ text: "Winston Light"},
					{ text: "L&M Loft"},
					{ text: "Winston Code"},
					{ text: "Winston Aqua"},
					{ text: "Next Light"},
					{ text: "Parliament"},
					{ text: "Other"}
				]);
			_listSmoking.dataProvider = groceryList;
			
			_listSmoking.listProperties.@itemRendererProperties.labelField = "text";
			_listSmoking.labelField = "text";
			_listSmoking.prompt = "בחר";
			_listSmoking.selectedIndex = -1;
			//_listSmoking.width = 400;
			//_listSmoking.height = 100;
			
			/////////////////////////
			//_listSmoking.dataProvider = _controller.mrSessions.compileDataProvider();
			
			_listSmoking.listProperties.itemRendererFactory = function():IListItemRenderer
			{
				var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
				renderer.labelField = "text";
				renderer.iconField = "thumbnail";
				renderer.labelFactory = trf;
				renderer.width = width*0.27*0.98;//width*0.9*0.96;
				renderer.defaultSkin = new Quad(1,1, 0xffffff);
				//renderer.defaultSkin.alpha = 0.8;
				renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_CENTER;
				renderer.iconPosition = Button.ICON_POSITION_RIGHT;
				return renderer;
			};
			
			_listSmoking.buttonFactory = function():Button
			{
				//var button:Button = SStarling.newButton("spriteSheet3.txa", "spriteSheet3.txa", "spriteSheet3.txa");
				//var bg:Quad = new Quad(200,100,0xffffff);
				//bg.alpha = 0.84;
				var bg:Image = CompsFactory.newImage("general::txa_red");
	
				var button:Button = CompsFactory.newButton(bg,bg);
				button.labelFactory = trf2;
				return button;
			};
			
			const listLayoutData:AnchorLayoutData = new AnchorLayoutData();
			listLayoutData.horizontalCenter = 0;
			listLayoutData.verticalCenter = 0;
			//this._mrDoctorPicker.layoutData = listLayoutData;
			
			//_mrDoctorPicker.listProperties.backgroundSkin = SStarling.newImage("spriteSheet3.BG");
			
			_listSmoking.listFactory = function():List
			{
				var list:List = new List();
				list.backgroundSkin = new Quad(1,1, _fontColor);//CompsFactory.newImage("general::txa_blue");
				
				
				var vl:VerticalLayout = new VerticalLayout();
				vl.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
				vl.gap = 1;
				list.horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
				list.width = width*0.27;//width*0.9*1;
				//list.height = 500;
				list.paddingTop = 2;
				list.paddingBottom = 2;
				
				list.layout = vl; 
				return list;
			};
			
			/*
			_mrDoctorPicker.listProperties.@itemRendererProperties.labelField = "text";
			_mrDoctorPicker.listProperties.@itemRendererProperties.iconSourceField = "thumbnail";
			*/
			_listSmoking.labelField = "text";
			//this._mrDoctorPicker.typicalItem = { text: "Select an Item" };
			
			_listSmoking.prompt = "בחר";
			_listSmoking.selectedIndex = -1;
			var popUpContentManager:VerticalCenteredPopUpContentManager = new VerticalCenteredPopUpContentManager();
			popUpContentManager.marginTop = 20;
			popUpContentManager.marginRight = 25;
			popUpContentManager.marginBottom = 20;
			popUpContentManager.marginLeft = 25;
			_listSmoking.popUpContentManager = popUpContentManager;
			_listSmoking.addEventListener( Event.CHANGE, list_changeHandler );
			
			/////////////////////////
			
			_tiSmoking = new FlexTextInput();
			_tiSmoking.backgroundSkin = img1;
			_tiSmoking.height = 78;
			_tiSmoking.width = 214;
			_tiSmoking.fontPercentHeight = 0.7;
			_tiSmoking.textAlign = TextFormatAlign.CENTER;
			_tiSmoking.color = 0xffffff;
			_tiSmoking.visible = false;
			
			_vGrpSmoking = new VGroup();
			_vGrpSmoking.addChild(_imgLbSmoking);
			_vGrpSmoking.addChild(_tiSmoking);
			//_vGrpSmoking.addChild(_listSmoking);
			_vGrpSmoking.percentWidth = 0.5;
			_vGrpSmoking.percentHeight = 1;
			
			var img2:Image = CompsFactory.newImage("general::txa_blue");
			_imgLbCode =  CompsFactory.newImage("general::txt_code");
			_tiCode = new FlexTextInput();
			_tiCode.restrict = "0-9";
			_tiCode.backgroundSkin = img2;
			_tiCode.height = 78;
			_tiCode.width = 214;
			_tiCode.fontPercentHeight = 0.7;
			_tiCode.textAlign = TextFormatAlign.CENTER;
			_tiCode.color = 0xffffff;
			
			_vGrpGender = new VGroup();
			_vGrpGender.addChild(_imgLbCode);
			_vGrpGender.addChild(_tiCode);
			_vGrpGender.percentWidth = 0.5;
			_vGrpGender.percentHeight = 1;
			
			_hGrpSmokingCode = new HGroup();
			
			_hGrpSmokingCode.addChild(_vGrpGender);
			_hGrpSmokingCode.addChild(_vGrpSmoking);
			_hGrpSmokingCode.percentWidth = 1;
			_hGrpSmokingCode.percentHeight = 0.27;
			_hGrpSmokingCode.gapPercentWidth = 0.1;
		}
		
		private function list_changeHandler():void
		{
			if (_listSmoking.selectedIndex == _listSmoking.dataProvider.length - 1)
			{
				_listSmoking.visible = false;
				_tiSmoking.visible = true;
			}
		}
		
		private function addAgeGender():void
		{
			var img1:Image = CompsFactory.newImage("general::txa_blue");
			_imgLbAge =  CompsFactory.newImage("general::txt_age");
			_tiAge = new FlexTextInput();
			_tiAge.backgroundSkin = img1;
			_tiAge.height = 78;
			_tiAge.width = 214;
			_tiAge.fontPercentHeight = 0.7;
			_tiAge.textAlign = TextFormatAlign.CENTER;
			_tiAge.color = 0xffffff;
			_tiAge.restrict = "0-9";
			_tiAge.textInitialIgnore = true;
			_tiAge.textInitial = " ";
			_vGrpAge = new VGroup();
			_vGrpAge.addChild(_imgLbAge);
			_vGrpAge.addChild(_tiAge);
			_vGrpAge.percentWidth = 0.5;
			_vGrpAge.percentHeight = 1;
			
			var img2:Image = CompsFactory.newImage("general::txa_red");
			_imgLbGender =  CompsFactory.newImage("general::txt_gender");
			_tiGender = new FlexTextInput();
			_tiGender.backgroundSkin = img2;
			_tiGender.height = 78;
			_tiGender.width = 214;
			_tiGender.fontPercentHeight = 0.7;
			_tiGender.textAlign = TextFormatAlign.CENTER;
			_tiGender.color = 0xffffff;
			_tiGender.isEditable = false;
			
			_btnFemale	=	CompsFactory.newButton("general::female_down", "general::female_normal", null, null, null, true,null,true,"right",false) as FlexButton;			
			_btnFemale.iconPosition = Button.ICON_POSITION_RIGHT;
			//_btnFemale.upIcon =_btnFemale.defaultIcon = new Quad(1,1,0xffffff);
			//_btnFemale.downIcon = CompsFactory.newImage("general::female_down");
			//_btnFemale.percentWidth	    =	1;
			//_btnFemale.percentHeight	=	1;
			
			_btnMale	=	CompsFactory.newButton("general::male_down", "general::male_normal",null, null, null, true,null,true,"right",false) as FlexButton;
			//_btnMale.upIcon =_btnMale.defaultIcon = new Quad(1,1,0xffffff);
			//_btnMale.downIcon = CompsFactory.newImage("general::male_down");
			_btnMale.iconPosition = Button.ICON_POSITION_RIGHT;
			//_btnMale.percentWidth	=	1;
			//_btnMale.percentHeight	=	1;
			
			
			_tgGender	=	new ToggleGroup();
			_tgGender.addItem(_btnMale);
			_tgGender.addItem(_btnFemale);
			_tgGender.selectedItem = null;
			_tgGender.addEventListener(Event.CHANGE, tg_onChange);
			
			_hGrpGndr = new HGroup();
			_hGrpGndr.addChild(_btnMale);
			_hGrpGndr.addChild(_btnFemale);
			_hGrpGndr.horizontalCenter = 0;
			_hGrpGndr.gapPercentWidth = 2;
			_hGrpGndr.top = 0;
			//_hGrpGndr.percentWidth = 0.4;
			//_hGrpGndr.percentHeight =0.2;
			
			
			_vGrpGender = new VGroup();
			_vGrpGender.addChild(_imgLbGender);
			_vGrpGender.addChild(_tiGender);
			_vGrpGender.percentWidth = 0.5;
			_vGrpGender.percentHeight = 1;
			
			_hGrpAgeGender = new HGroup();
			
			_hGrpAgeGender.addChild(_vGrpGender);
			_hGrpAgeGender.addChild(_vGrpAge);
			_hGrpAgeGender.percentWidth = 1;
			_hGrpAgeGender.percentHeight = 0.27;
			_hGrpAgeGender.gapPercentWidth = 0.1;
		}
		
		private function tg_onChange():void
		{
			if (_btnMale.isSelected)
				_gender = "Male";
			else if (_btnFemale.isSelected)
				_gender = "Female";
			
			trace("_gender is = " + _gender);
		}
		
		private function btnNext_onTriggered():void
		{
			_tiAge.validate();
			_listSmoking.validate();
			_tiCode.validate();
			
			var smokingBrand:String = "";
			if (_errorMsg == "" && _gender == "")
				_errorMsg = "ןימ רוחבל הבוח";
			if (_errorMsg == "" && (_tiAge.text == "" || _tiAge.text == null) )
				_errorMsg = "ליג רוחבל הבוח";
			if (_errorMsg == "" && parseInt(_tiAge.text) < 18)
				_errorMsg = "!דבלב 18 ליג לעמ תופתתשהה";
			if (_errorMsg == "" && !_isAgree)
				_errorMsg = "תופתתשה ןונקת רשאל הבוח";
			if ((_errorMsg == "" && _listSmoking.selectedIndex == -1) || ((_listSmoking.selectedIndex == _listSmoking.dataProvider.length - 1) && _tiSmoking.text == ""))
				_errorMsg = "גתומ רוחבל הבוח";
			if (_errorMsg == "" && !isValidUserEmail(_tiEmail.text) )
				_errorMsg = "תיקוח ליימ תבותכ סינכהל שי";
			if (_errorMsg == "" && _tiCode.text == "")
				_errorMsg = "דוק סינכהל הבוח";
			if (!_isConnected)
				_errorMsg = "טנרטניאל רוביחב האיגש";
			
				//	QuestionierData.instance.addRecord();
			
			if (_errorMsg != "")
			{
				_ppFields = null;
				_ppFields											= new PopupWarning();
				_ppFields.onAction									= popup_onAction;
				//_ppFields.visible 								= true;
				
				_ppFields.textYes									= "ok";
				_ppFields.textHeadline								= "םירסח תודש";
				_ppFields.width = 1280 / 2.0;
				_ppFields.height = 800 / 2.0;
				_ppFields.textWarning = _errorMsg;
				_ppFields.validate();
				addChild(_ppFields);

				return;
			}
			
			
			
			this._appController.playSound("next");
			
			if (_listSmoking.selectedIndex < _listSmoking.dataProvider.length - 1)
				smokingBrand = _listSmoking.dataProvider.getItemAt(_listSmoking.selectedIndex).text as String;
			else
				smokingBrand = _tiSmoking.text;
			
			QuestionierData.instance.updateParticipantDetails(_tiAge.text, 
															 _gender, 
													   	 	  smokingBrand,
													  		 _tiCode.text,
													   		 _tiEmail.text,
															 _isAgree ? "true" : "false");
			
			QuestionierData.instance.addRecord();
			_appController.pushScreen("sView_MaleFemaleScreen");
		}
		
		override protected function draw():void
		{
			super.draw();
			
			_vGrpAll.validate();
			_tiSmoking.validate();
			
			_listSmoking.width  = _tiSmoking.width;
			_listSmoking.height  = _tiSmoking.height;
			_listSmoking.y  = _vGrpAll.y + _hGrpSmokingCode.y + _vGrpSmoking.y + _tiSmoking.y;
			//_listSmoking.alpha = 0;
			//_listSmoking.x = -200;
			
			_tiGender.validate();
			_hGrpAgeGender.validate();
			
			_hGrpGndr.width = _tiGender.width;
			_hGrpGndr.height = _tiGender.height;
			
			_hGrpGndr.y  = _vGrpAll.y + _hGrpAgeGender.y + _imgLbGender.y + _tiGender.y + 5;
			_btnFemale.width  =	0.3* _tiGender.width;
			_btnFemale.height	= 0.85*_tiGender.height;
			_btnMale.width  =	0.3* _tiGender.width;
			_btnMale.height	= 0.85*_tiGender.height;
			
			if (_ppFields)
			{
				_ppFields.x = (width - _ppFields.width) / 2.0; 
				_ppFields.y = (height - _ppFields.height) / 2.0;
			}
			
			if (!_animationDone)
			{
				_animationDone = true;
				
				_vGrpAll.validate();
				_hGrpGndr.validate();
				var tween:Tween = new Tween(_vGrpAll, 1, Transitions.EASE_IN_BACK);
				var tween1:Tween = new Tween(_listSmoking, 1, Transitions.EASE_IN_BACK);
				var tween2:Tween = new Tween(_hGrpGndr, 1, Transitions.EASE_IN_BACK);
				var currX:int = _vGrpAll.x;
				_vGrpAll.x = -_vGrpAll.width;
				_vGrpAll.alpha = 0;
				_hGrpGndr.x = -100;
				_hGrpGndr.alpha = 0;
				
				tween.animate("x", 100);
				tween.animate("alpha", 1);
				tween1.animate("x", 100 + _hGrpSmokingCode.x + _vGrpSmoking.x + _tiSmoking.x);
				tween1.animate("alpha", 1);
				tween2.animate("x", 70);
				tween2.animate("alpha", 1);
				
				var tween3:Tween = new Tween(_cigarettes, 1, Transitions.EASE_IN_BACK);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween3.animate("x", cigX);
				tween3.animate("alpha", 1);
				
				//tween.onComplete = function():void { trace("tween complete!"); };
				Starling.juggler.add(tween);
				Starling.juggler.add(tween1);
				Starling.juggler.add(tween2);
				Starling.juggler.add(tween3);

			}
			
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
		
	}
}