package views
{
	import com.mreshet.mrComps.flexComps.flexTextInput.FlexTextInput;
	
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import starling.display.Quad;
	
	import views.base.ScreenBase;

	public class View_Screen2 extends ScreenBase
	{
		private var _quad:			Quad 				= null;
		private var _tiTest:		FlexTextInput	=	null;
		
				
    public function View_Screen2()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
			
		}
		
		override protected function initialize():void
		{
			
			super.initialize();

			var tf:TextFormat	=	new TextFormat("FbAgas-Regular2", 22,0x00ff00);
			var tf2:TextFormat	=	new TextFormat("FbAgas-Regular2", 22,0x00ff00);
			var tf3:TextFormat	=	new TextFormat("FbAgas-Regular2", 22,0x00ff00);
			
			_quad	=	new Quad(1,1, 0x028983);
			
			//
			var bg:Quad = new Quad(1,1,0xffffff);
			bg.alpha = 0.84;
			_tiTest = new FlexTextInput();
			_tiTest.backgroundSkin = bg;
			_tiTest.percentHeight = 15;
			_tiTest.percentWidth = 60;
			_tiTest.fontPercentHeight = 0.8;
			_tiTest.textAlign = TextFormatAlign.LEFT;
			
			addChild(_quad);
			addChild(_tiTest);
		}
				
		override protected function draw():void
		{
			super.draw();
			_quad.width = width;
			_quad.height = height;

		}
				
	    protected override function onBackButton():void
	    {
	      _appController.previousScreen();
	    }

	}
}