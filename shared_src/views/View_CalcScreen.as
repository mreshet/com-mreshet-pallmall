package views
{
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import starling.animation.Tween;
	import starling.core.Starling;

	
	import views.base.ScreenBase;
	
	public class View_CalcScreen extends ScreenBase
	{
		private var _imgBg:					FlexImage	= null;
		private var _btnNext:				FlexButton	= null;
		private var _cigarettes:			FlexImage	= null;
		private var _tasteImg:				FlexImage	= null;
		private var _titleImg:				FlexImage	= null;
		private var _animationDone: 		Boolean		= false;
		private var _cntr:			 		int			= 0;
		
		public function View_CalcScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		override protected function initialize():void
		{
			
			super.initialize();
			_cntr = 0;
			_imgBg					=	new FlexImage();
			_imgBg.scaleMode 		= FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	=	100;
			_imgBg.percentWidth		=	100;
			_imgBg.source			=	"general::bg";
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	  =	331;
			_cigarettes.width     =	441;
			_cigarettes.source	  =		"general::cigarettes";
			_cigarettes.right 	  = 50;
			_cigarettes.bottom 	  = 75;
			
			_tasteImg = new FlexImage();
			_tasteImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_tasteImg.source	=	"general::taste";
			_tasteImg.height	=	181;
			_tasteImg.width    =	386;
			_tasteImg.right = -10;
			_tasteImg.top = 100;
			
			_titleImg = new FlexImage();
			_titleImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_titleImg.source	=	"general::calc1";
			_titleImg.height	=	370;
			_titleImg.width    =	783;
			_titleImg.left = -10;
			_titleImg.top = 50;
			
			addChild(_imgBg);
			
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			addChild(_tasteImg);
			addChild(_backBtn);
			addChild(_cigarettes);
			addChild(_titleImg);
			addChild(_internet);
			addChild(_exitBtn);
		}
		
		
		
		private function startTimer():void 
		{
			var myTimer:Timer = new Timer(600, 3);
			myTimer.addEventListener("timer", timerHandler);
			myTimer.start();
		}
		
		public function timerHandler(event:TimerEvent):void 
		{
			trace("timerHandler: " + event);
			trace("_cntr: " + _cntr);
			
			_cntr++;	
			if (_cntr == 1)	
				_titleImg.source	=	"general::calc2"; 
			else if (_cntr == 2) 
				_titleImg.source	=	"general::calc3"; 
			else 
				_appController.pushScreen("sView_ResultsScreen");
		}
		
		
		override protected function draw():void
		{
			super.draw();
			
			if (!_animationDone)
			{
				//Cigarettes
				_animationDone = true;
				_cigarettes.validate();
				var tween3:Tween = new Tween(_cigarettes, 1);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween3.animate("x", cigX);
				tween3.animate("alpha", 1);
				Starling.juggler.add(tween3);
				
				//Taste
				var tween2:Tween = new Tween(_tasteImg, 1.1);
				var tasteX:int = _tasteImg.x;
				_tasteImg.x += _tasteImg.width;
				_tasteImg.alpha = 0;
				tween2.animate("x", tasteX);
				tween2.animate("alpha", 1);
				Starling.juggler.add(tween2);
				
				//Title
				var tween1:Tween = new Tween(_titleImg, 1);
				tween1.onComplete = function():void { trace("compleeeeeeeet"); startTimer(); };
				var titleX:int = _titleImg.x;
				_titleImg.x -= _titleImg.width;
				_titleImg.alpha = 0;
				tween1.animate("x", titleX);
				tween1.animate("alpha", 1);
				Starling.juggler.add(tween1);
			}
		}
		
		private function updateTitle():void
		{
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
		
	}
}