package views
{
	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	
	import app.QuestionierData;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	
	import views.base.ScreenBase;
	
	public class View_ResultsScreen extends ScreenBase
	{
		private var _imgBg:					FlexImage	= null;
		private var _titleImg:				FlexImage	= null;
		private var _resImg:				FlexImage	= null;
		private var _btnNext:				FlexButton	= null;
		private var _animationDone: 		Boolean		= false;
		private var _cigarettes:			FlexImage	= null;
		private var _tasteImg:				FlexImage	= null;
		
		public function View_ResultsScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		override protected function initialize():void
		{
			super.initialize();
			
			_titleImg				= new FlexImage();
			_titleImg.scaleMode 	= FlexImage.SCALEMODE_STRECTH;
			_titleImg.percentHeight	= 100;
			_titleImg.percentWidth	= 100;
			_titleImg.source		= "general::calc";
			
			_resImg					= new FlexImage();
			_resImg.scaleMode 		= FlexImage.SCALEMODE_LETTERBOX;
			_resImg.topPercentHeight = 0.1;
			_resImg.percentHeight	= 45;
			_resImg.percentWidth	= _resImg.percentHeight * 2.116216216216216;
			
			if (QuestionierData.instance.getScore() < 4)
				_resImg.source			= "general::res1";
			else if (QuestionierData.instance.getScore() < 7)
				_resImg.source			= "general::res2";
			else 
				_resImg.source			= "general::res3";
			
			_imgBg					= new FlexImage();
			_imgBg.scaleMode 		= FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	= 100;
			_imgBg.percentWidth		= 100;
			_imgBg.source			= "general::bg";
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	=	331;
			_cigarettes.width    =	441;
			_cigarettes.source	=	"general::cigarettes";
			_cigarettes.right = 50;
			_cigarettes.bottom = 75;
			
			_tasteImg = new FlexImage();
			_tasteImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_tasteImg.source	=	"general::taste";
			_tasteImg.height	=	181;
			_tasteImg.width    =	386;
			_tasteImg.right = 420;
			_tasteImg.bottom = 130;
			
			_btnNext							= CompsFactory.newButton(null, null, btnNext_onTriggered, null, null, false, "general::btn_next_up", true,"center") as FlexButton;
			_btnNext.downIcon					= CompsFactory.newImage("general::btn_next_down");
			
			_btnNext.percentWidth				= 108.0 / 1280;
			_btnNext.percentHeight				= 57.0 / 800;
			_btnNext.topPercentHeight 			= 650.0 / 800;
			_btnNext.leftPercentWidth			= 50 / 1280;
			_btnNext.iconPercentHeight 			= 1;
			_btnNext.relativeCalcHeightParent	= this;
			_btnNext.relativeCalcWidthParent	= this;
			
			addChild(_imgBg);
			
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			addChild(_btnNext);
			addChild(_resImg);
			addChild(_tasteImg);
			addChild(_cigarettes);
			addChild(_backBtn);
			addChild(_internet);
			
			this._appController.playSound("smooth");
		}
		
		private function btnNext_onTriggered():void
		{
			this._appController.playSound("next");
			_appController.pushScreen("sView_EndScreen");
		}
		
		override protected function draw():void
		{
			super.draw();
			
			if (!_animationDone)
			{
				//Cigarettes
				_animationDone = true;
				//Title
				var tween1:Tween = new Tween(_resImg, 1);
				var titleX:int = _resImg.x;
				_resImg.x -= _resImg.width;
				_resImg.alpha = 0;
				tween1.animate("x", titleX);
				tween1.animate("alpha", 1);
				Starling.juggler.add(tween1);
				
				//Cigarettes
				var tween3:Tween = new Tween(_cigarettes, 1);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween3.animate("x", cigX);
				tween3.animate("alpha", 1);
				Starling.juggler.add(tween3);
				
				//Taste
				var tween2:Tween = new Tween(_tasteImg, 1.1);
				var tasteX:int = _tasteImg.x;
				_tasteImg.x += _tasteImg.width;
				_tasteImg.alpha = 0;
				tween2.animate("x", tasteX);
				tween2.animate("alpha", 1);
				Starling.juggler.add(tween2);
				
			}
			
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
	}
}