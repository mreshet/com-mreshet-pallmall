package views
{

	import com.mreshet.mrComps.CompsFactory;
	import com.mreshet.mrComps.flexComps.flexButton.FlexButton;
	import com.mreshet.mrComps.flexComps.flexImage.FlexImage;
	import com.mreshet.mrComps.flexComps.vGroup.VGroup;
	
	import app.QuestionierData;
	import feathers.layout.HorizontalLayout;
	import starling.animation.Tween;
	import starling.core.Starling;
	import views.base.ScreenBase;
	
	public class View_MaleFemaleScreen extends ScreenBase
	{
		private var _imgBg:					FlexImage	= null;
		private var _cigarettes:			FlexImage	= null;
		
		//private var _womenImg:				FlexImage		=	null;
		//private var _menImg:				FlexImage		=	null;
		
		private var _btnWomen:				FlexButton		= null;
		private var _btnMen:				FlexButton		= null;
		private var _vGrpBtns:				VGroup 			= null;
		private var _animationDone: 		Boolean			= false;
		private var _tasteImg:				FlexImage		= null;
		
		public function View_MaleFemaleScreen()
		{
			super();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		override protected function initialize():void
		{
			
			super.initialize();
			
			_imgBg					=	new FlexImage();
			_imgBg.scaleMode 		= FlexImage.SCALEMODE_STRECTH;
			_imgBg.percentHeight	=	100;
			_imgBg.percentWidth		=	100;
			_imgBg.source		=	"general::bg";
			
			_cigarettes = new FlexImage();
			_cigarettes.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_cigarettes.height	=	331;
			_cigarettes.width    =	441;
			_cigarettes.source	=	"general::cigarettes";
			_cigarettes.right = 50;
			_cigarettes.bottom = 75;
			
			_tasteImg = new FlexImage();
			_tasteImg.scaleMode = FlexImage.SCALEMODE_STRECTH;
			_tasteImg.source	=	"general::taste";
			_tasteImg.height	=	181;
			_tasteImg.width    =	386;
			_tasteImg.right = 420;
			_tasteImg.bottom = 130;
			
			
			_btnWomen							= CompsFactory.newButton(null, null, btnWomen_onTriggered, null, null, false, "general::women", true,"left") as FlexButton;
			//_btnWomen.downIcon					= CompsFactory.newImage("general::btn_next_down");
			_btnWomen.percentWidth				= 55;
			_btnWomen.percentHeight				= 20;
			_btnWomen.horizontalAlign 			= HorizontalLayout.HORIZONTAL_ALIGN_LEFT;
			_btnWomen.iconPercentHeight 		= 1;
			_btnWomen.leftPercentWidth			= -0.02;
			_btnWomen.relativeCalcHeightParent	= this;
			_btnWomen.relativeCalcWidthParent	= this;
			
			_btnMen								= CompsFactory.newButton(null, null, btnMen_onTriggered, null, null, false, "general::men", true,"right") as FlexButton;
			//_btnWomen.downIcon					= CompsFactory.newImage("general::btn_next_down");
			_btnMen.percentWidth				= 55;
			_btnMen.percentHeight				= 20;
			_btnMen.horizontalAlign 			= HorizontalLayout.HORIZONTAL_ALIGN_RIGHT;
			_btnMen.iconPercentHeight 			= 1;
			_btnMen.rightPercentWidth			= -0.02;
			_btnMen.relativeCalcHeightParent	= this;
			_btnMen.relativeCalcWidthParent		= this;
			
			_vGrpBtns = new VGroup();
			_vGrpBtns.addChild(_btnWomen);
			_vGrpBtns.addChild(_btnMen);
			//_vGrpBtns.leftPercentWidth = 0.08;
			_vGrpBtns.topPercentHeight = 0.04;
			_vGrpBtns.gapPercentHeight = 0.00;
			//_vGrpAll.top = 100;
			_vGrpBtns.percentHeight = 30;
			_vGrpBtns.percentWidth = 100;
			
			addChild(_imgBg);
			
			addChild(_mcBlue);
			addChild(_mcRed);
			addChild(_mcWhite2);
			addChild(_mcLight);
			addChild(_mcLight2);
			
			addChild(_tasteImg);
			addChild(_vGrpBtns);
			addChild(_backBtn);
			addChild(_cigarettes);
			addChild(_internet);
			addChild(_exitBtn);
		}
		
		private function btnMen_onTriggered():void
		{
			QuestionierData.instance.updateParticipantGenger("Men");
			_appController.pushScreen("sView_WelcomeScreen");
			
		}
		
		private function btnWomen_onTriggered():void
		{
			QuestionierData.instance.updateParticipantGenger("Women");
			_appController.pushScreen("sView_WelcomeScreen");
		}
		
		override protected function draw():void
		{
			super.draw();
			
			if (!_animationDone)
			{
				_btnWomen.validate();
				_btnMen.validate();
				_animationDone = true;
				var tween:Tween = new Tween(_btnWomen, 1);
				var tween1:Tween = new Tween(_btnMen, 1);
				
				var currXMen:int = _btnMen.x;
				var currXWomen:int = _btnWomen.x;
				_btnMen.x += _btnMen.width;
				_btnWomen.x -= _btnWomen.width;
				_btnMen.alpha = 0;
				_btnWomen.alpha = 0;
				
				tween.animate("x", currXWomen);
				tween.animate("alpha", 1);
				tween1.animate("x", currXMen);
				tween1.animate("alpha", 1);
				
				//Taste
				var tween2:Tween = new Tween(_tasteImg, 1.1);
				var tasteX:int = _tasteImg.x;
				_tasteImg.x += _tasteImg.width;
				_tasteImg.alpha = 0;
				tween2.animate("x", tasteX);
				tween2.animate("alpha", 1);
				Starling.juggler.add(tween2);
				
				tween.onComplete = function():void { trace("tween complete!"); };
				Starling.juggler.add(tween);
				Starling.juggler.add(tween1);
				
				var tween4:Tween = new Tween(_cigarettes, 1);
				var cigX:int = _cigarettes.x;
				_cigarettes.x += _cigarettes.width;
				_cigarettes.alpha = 0;
				tween4.animate("x", cigX);
				tween4.animate("alpha", 1);
				Starling.juggler.add(tween4);
			}
			
		}
		
		protected override function onBackButton():void
		{
			_appController.previousScreen();
		}
		
	}
}