package events
{
	import flash.events.Event;
	
	public class ControllerEvent extends Event
	{
		public static const READY							: String = "READY";

		public function ControllerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}