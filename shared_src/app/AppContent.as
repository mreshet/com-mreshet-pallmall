package app
{
	import com.mreshet.mrContentManager.MrContentManager;
	import com.mreshet.mrContentManager.MrPackage;
	import com.mreshet.mrContentManager.core.manifest.LocalResource;
	
	public class AppContent extends MrContentManager
	{
		public var cb_onInitalContentLoaded:Function = null;
		
		public function AppContent()
		{
			super();
		}
		
		/**
		 * here deal with content processing like parsing etc for initial loading..
		 */
		public function loadInitialContent($cb_onInitalContentLoaded:Function = null):void
		{
			cb_onInitalContentLoaded 	= $cb_onInitalContentLoaded;
			
			var newBatch:MrPackage 		= addOrGetContentPackage("initBatch1");
			
			newBatch.enqueue("assets/data.xml", "appConfig1");
			//newBatch.enqueue("assets/packages/content/logo_box.png", "logo_box", LocalResource.TYPE_BITMAP);
			newBatch.enqueue("assets/sounds/next.mp3", "next", LocalResource.TYPE_SOUND);
			newBatch.enqueue("assets/sounds/question.mp3", "question", LocalResource.TYPE_SOUND);
			newBatch.enqueue("assets/sounds/smooth.mp3", "smooth", LocalResource.TYPE_SOUND);
			//newBatch.enqueue("assets/packages/content/shareBitmap.png", "shareBitmap", LocalResource.TYPE_BITMAP);
			newBatch.process(onInitBatchFinished);
		}
		
		private function onInitBatchFinished($pack:MrPackage):void
		{
			if(cb_onInitalContentLoaded is Function)
				cb_onInitalContentLoaded();
			
			//playSound("initBatch1.next");
		}
		
	}
}