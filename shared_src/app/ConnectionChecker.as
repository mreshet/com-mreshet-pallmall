package app
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	[Event(name="error", type="flash.events.Event")]
	[Event(name="success", type="flash.events.Event")]
	public class ConnectionChecker extends EventDispatcher
	{
		public static const EVENT_SUCCESS:String = "success";
		public static const EVENT_ERROR:String = "error";
		
		// Though google.com might be an idea, it is generally a better practice
		// to use a url with known content, such as http://foo.com/bar/mytext.txt
		// By doing so, known content can also be verified.
		
		// This would make the checking more reliable as the wireless hotspot sign-in
		// page would negatively intefere the result.
		private var _urlToCheck:String = "http://cigarette.mreshet.co.il";
		
		// empty string so it would always be postive
		private var _contentToCheck:String = "";
		private var _loader:URLLoader = new URLLoader();
		
		public function ConnectionChecker()
		{
			super();
			_loader.dataFormat = URLLoaderDataFormat.TEXT;	
			
		}
		
		public function check():void
		{
			var urlRequest:URLRequest = new URLRequest(_urlToCheck);
			try
			{
				_loader.addEventListener(Event.COMPLETE, loader_complete);
				_loader.addEventListener(IOErrorEvent.IO_ERROR, loader_error);
				_loader.load(urlRequest);
			}
			catch ( e:Error )
			{
				trace("e = " + e.message);
				dispatchErrorEvent();
			}
		}
		
		private function loader_complete(event:Object):void
		{
			_loader.removeEventListener(Event.COMPLETE, loader_complete);
			_loader.removeEventListener(IOErrorEvent.IO_ERROR, loader_error);
			
			var textReceived:String = _loader.data as String;
			
			if ( textReceived )
			{
				dispatchSuccessEvent();
			}
			else
			{
				dispatchErrorEvent();
			}
		}
		
		private function loader_error(event:Object):void
		{
			dispatchErrorEvent();
		}
		
		private function dispatchSuccessEvent():void
		{
			dispatchEvent( new Event( EVENT_SUCCESS ) );
		}
		
		private function dispatchErrorEvent():void
		{
			dispatchEvent( new Event( EVENT_ERROR ) );
		}
	}
}