package app
{
	import com.mreshet.mrComps.bitmapLayersComposer.BitmapLayersComposer;
	import com.mreshet.mrComps.mrScreenNavigator.MrScreenNavigator;
	import com.mreshet.mrContentManager.core.manifest.LocalResource;
	import com.mreshet.mrContentManager.core.types.SoundContent;
	import com.mreshet.mrGfxManager.MrGfxManager;
	import com.mreshet.mrGfxManager.MrGfxPackage;
	import com.mreshet.mrUtils.MrExtTimer;
	
	import flash.desktop.NativeApplication;
	import flash.display.DisplayObjectContainer;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.system.Capabilities;
	
	import assets.SEmbeddedAssets;
	
	import events.ControllerEvent;
	
	import feathers.controls.ScreenNavigatorItem;
	
	import starling.display.DisplayObject;
	
	public class AppController extends EventDispatcher
	{

		/** Controller */
		private static var _instance:			AppController					=	null;
		
		/** View: Main Window, and navigator */
		private var _app:									Object								=	null;
		private var _appMainWindow:				AppMainWindow					=	null; 
		private var _nav:									MrScreenNavigator			=	null;
		
		/** Model */
		private var _appContent:					AppContent						=	null;
				
		private var _appSplash:						BitmapLayersComposer	=	null;
		
		private var _gfxManager:					MrGfxManager 					= null;
				
    //private var _gaTracker:           AnalyticsTracker  		= null;

		/** extras */
		
		public function AppController()
		{
			if(_instance	!=	null)
				throw new Error("AppController is singleton!");
			
			/** First instanciation */
			_instance					=	this;
		}
		
		public static function get instance():AppController
		{
			return ((_instance) ? _instance : new AppController());
		}

		/**
		 * Content Methods
		 */
		public function contentStart($app:Object = null)	:	void
		{
			_app 						= $app;
			_appMainWindow	=	(_app as AppMainStarling).mainWindow;

			//_gaTracker  		= new GATracker(Starling.current.nativeStage, "UA-46937888-1", "AS3" );

			_gfxManager 		= MrGfxManager.instance;
			
			_appContent 		= new AppContent();
			_appContent.loadInitialContent(onInitialContentLoaded);
		}
		
		private function ready($obj:Object = null):void
		{
			dispatchEvent(new ControllerEvent(ControllerEvent.READY));	
		}
		
		private function onInitialContentLoaded():void
		{
			trace("content has loaded!!!");
			loadGfxPacks();
		}
		
		private function loadGfxPacks():void
		{
			var mrp:	MrGfxPackage 					= _gfxManager.addOrGetContentPackage("general") as MrGfxPackage;

			var mrpAnima:	MrGfxPackage 					= _gfxManager.addOrGetContentPackage("animation") as MrGfxPackage;
			
			mrpAnima.enqueue("assets/packages/animations/animationBlue.png", 			 "animationBlue",  LocalResource.TYPE_BITMAP);
			mrpAnima.enqueue("assets/packages/animations/animationBlue.xml", 			 "animationBlueXML");

			mrpAnima.enqueue("assets/packages/animations/animationRed.png", 			 "animationRed",   LocalResource.TYPE_BITMAP);
			mrpAnima.enqueue("assets/packages/animations/animationRed.xml", 			 "animationRedXML" );

			//mrpAnima.enqueue("assets/packages/animations/animationWhite.png", 			 "animationWhite",  LocalResource.TYPE_BITMAP);
			//mrpAnima.enqueue("assets/packages/animations/animationWhite.xml", 			 "animationWhiteXML");

			mrpAnima.enqueue("assets/packages/animations/animationWhite2.png", 			 "animationWhite2",  LocalResource.TYPE_BITMAP);
			mrpAnima.enqueue("assets/packages/animations/animationWhite2.xml", 			 "animationWhite2XML");
			
			mrpAnima.enqueue("assets/packages/animations/lightexplo.png", 			  	 "lightexplo",  LocalResource.TYPE_BITMAP);
			mrpAnima.enqueue("assets/packages/animations/lightexplo.xml", 			 	 "lightexploXML");
			
			mrp.loadTexturesAutomatically 	= true;
			
			mrp.enqueue("assets/packages/general/bg.png", 			 "bg", 				LocalResource.TYPE_BITMAP);
			//mrp.enqueue("assets/packages/general/BG4.png", 			 "bg4",				LocalResource.TYPE_BITMAP);
			//mrp.enqueue("assets/packages/general/BG17.png", 		 "bg17",			LocalResource.TYPE_BITMAP);
			//mrp.enqueue("assets/packages/general/BG21.png", 		 "bg21",			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/btn_next_down.png", "btn_next_down",	LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/btn_next_up.png", 	 "btn_next_up", 	LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_name.png", 	 "txt_name", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_date.png", 	 "txt_date", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_city.png", 	 "txt_city", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_age.png", 	 	 "txt_age", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_gender.png", 	 "txt_gender", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txa_blue.png", 	 "txa_blue", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txa_light_blue.png","txa_light_blue", 	LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txa_red.png", 		 "txa_red", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_brand.png", 	 "txt_brand", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_code.png", 	 "txt_code", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_mail.png", 	 "txt_mail", 		LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/txt_agree1.png", 	 "txt_agree1", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_agree2.png", 	 "txt_agree2", 		LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/women.png", 	 	 "women", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/men.png", 	 		 "men", 			LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/5_1.png", 	 		 "5_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/5_2.png", 	 		 "5_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/5_3.png", 	 		 "5_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/6_1.png", 	 		 "6_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/6_2.png", 	 		 "6_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/6_3.png", 	 		 "6_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/7_1.png", 	 		 "7_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/7_2.png", 	 		 "7_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/7_3.png", 	 		 "7_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/8_1.png", 	 		 "8_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/8_2.png", 	 		 "8_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/8_3.png", 	 		 "8_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/9_1.png", 	 		 "9_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/9_2.png", 	 		 "9_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/9_3.png", 	 		 "9_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/10_1.png", 	 	 "10_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/10_2.png", 	 	 "10_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/10_3.png", 	 	 "10_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/11_1.png", 	 	 "11_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/11_2.png", 	 	 "11_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/11_3.png", 	 	 "11_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/12_1.png", 	 	 "12_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/12_2.png", 	 	 "12_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/12_3.png", 	 	 "12_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/13_1.png", 	 	 "13_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/13_2.png", 	 	 "13_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/13_3.png", 	 	 "13_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/14_1.png", 	 	 "14_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/14_2.png", 	 	 "14_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/14_3.png", 	 	 "14_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/15_1.png", 	 	 "15_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/15_2.png", 	 	 "15_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/15_3.png", 	 	 "15_3", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/16_1.png", 	 	 "16_1", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/16_2.png", 	 	 "16_2", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/16_3.png", 	 	 "16_3", 			LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/title5.png", 	 	 "title5", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title6.png", 	 	 "title6", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title7.png", 	 	 "title7", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title8.png", 	 	 "title8", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title9.png", 	 	 "title9", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title10.png", 	 	 "title10", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title11.png", 	 	 "title11", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title12.png", 	 	 "title12", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title13.png", 	 	 "title13", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title14.png", 	 	 "title14", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title15.png", 	 	 "title15", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/title16.png", 	 	 "title16", 		LocalResource.TYPE_BITMAP);
			
			
			mrp.enqueue("assets/packages/general/res1.png", 	 	 "res1", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/res2.png", 	 	 "res2", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/res3.png", 	 	 "res3", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/calc3.png", 	 	 "calc3", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/calc2.png", 	 	 "calc2", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/calc1.png", 	 	 "calc1", 		LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/female_normal.png", 	 "female_normal", 	LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/female_down.png", 	 	 "female_down", 	LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/male_normal.png",	 	 "male_normal",		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/male_down.png", 	 	 "male_down", 		LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/cigarettes.png", 	 	 "cigarettes", 		LocalResource.TYPE_BITMAP);
			//mrp.enqueue("assets/packages/general/cigarettes_big.png", 	 "cigarettes_big", 		LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/txt_welcome.png", 	 	 "txt_welcome", 	LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/txt_smooth.png", 	 	 "txt_smooth", 		LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/taste.png", 	 		 "taste", 			LocalResource.TYPE_BITMAP);
			
			mrp.enqueue("assets/packages/general/price24.png", 	 		 "price24", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/filter.png", 	 		 "filter", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/taste_heb.png", 	 	 "taste_heb", 			LocalResource.TYPE_BITMAP);
			
			
			mrp.enqueue("assets/packages/general/on.png", 	 	 "on", 			LocalResource.TYPE_BITMAP);
			mrp.enqueue("assets/packages/general/off.png", 	 	 "off", 			LocalResource.TYPE_BITMAP);
			
			
			_gfxManager.loadPackages("*", onGfxLoaded);
		}
		
		private function onGfxLoaded($obj:Object = null):void
		{
			ready();
		}
		
    /**
     * GA interface 
     */
    
    public function gaTrackPageview($trackStr:String, $delay:uint = 1000):void
    {
      var timer:  MrExtTimer  = new MrExtTimer($delay, 1);
      
      timer.message           = $trackStr;
      timer.addEventListener(TimerEvent.TIMER_COMPLETE, onGaTimerComplete);
      
      timer.start(); 
    }
    
    protected function onGaTimerComplete(event:TimerEvent):void
    {
			(event.currentTarget as MrExtTimer).removeEventListener(TimerEvent.TIMER_COMPLETE, onGaTimerComplete);
      		//_gaTracker.trackPageview((event.currentTarget as MrExtTimer).message);
    }
    
		/**
		 * Navigator Methods 
		 */		
		public function previousScreen(): void
		{
			_appMainWindow.navigator.previousScreen();
		}
		
		public function pushScreen($id: String): void
		{
			_appMainWindow.navigator.pushScreen($id);
		}
		
		public function getScreenById($id:String):ScreenNavigatorItem
		{
			return _appMainWindow.navigator.getScreenNavigatorItemById($id);
		}
		
		public function get activeScreenID():String
		{
			return _appMainWindow.navigator.activeScreenID;
		}
		
		public function get activeScreen():DisplayObject
		{
			return _appMainWindow.navigator.activeScreen;
		}
		
    public function exitApplication():void
    {
      NativeApplication.nativeApplication.exit();
    }

		/**
		 * extras
		 */
		public function startSplash($parent:DisplayObjectContainer):void
		{
			_appSplash = new BitmapLayersComposer($parent);
			
			_appSplash.dataProvider = Vector.<Object>([
				{id:"1",  src: SEmbeddedAssets._bm_BG, percentWidth: 100, percentHeight: 100, scaleMode: BitmapLayersComposer.SCALEMODE_STRECTH, bottom: NaN, top:NaN, left:NaN ,right:NaN, horizontalCenter:0, verticalCenter:0},
				//{id:"2",  src: SEmbeddedAssets._bm_logo, percentWidth: 60, percentHeight: 100, scaleMode: BitmapLayersComposer.SCALEMODE_LETTERBOX, bottom: NaN, top:NaN, left:NaN ,right:NaN, horizontalCenter:0, verticalCenter:-40},
				//{id:"3",  src: SEmbeddedAssets._bm_strip, percentWidth: 100, percentHeight: 100, scaleMode: BitmapLayersComposer.SCALEMODE_LETTERBOX, bottom: 0, top:NaN, left:NaN ,right:NaN, horizontalCenter:NaN, verticalCenter:NaN}
			]);
			
			_appSplash.start();
			
		}
		
		public function removeSplash():void
		{
			if(_appSplash)
				_appSplash.remove();
			_appSplash = null;			
		}
		
		public function playSound($id:String):void
		{
			//_appContent.playSound($id);
			var sc:SoundContent = _appContent.getContentById("initBatch1::" + $id) as SoundContent;
			if (sc)
				sc.stopAndplay();
			
		}
		
		public function toggleTabBarVisibility($visible:Boolean):void
		{
			_appMainWindow.toggleTabBarVisibility($visible);
		}

		public function showExitWarning():void
		{
			_appMainWindow.showWarning();	
		}
		
		public function detectiOSversion():int
		{
			var os:					String 	= Capabilities.os;
			
			var iosString:	String 	= "iPhone OS ";
			
			var index:			int 		= os.indexOf(iosString); //iPhone OS 6.1.3
			
			if(index == -1)
				return -1;
	
			index										=	 index + iosString.length;
			
			return int(os.charAt(index));
		}
		
		/**
		 * Getters and Setters
		 */
		
		public function get appMain():										Object						{	return _app;						}

		public function get nav():												MrScreenNavigator	{	return _appMainWindow.navigator;	}

		public function get appContent():									AppContent				{	return _appContent;			}

  }
}