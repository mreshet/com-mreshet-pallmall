package app
{
	import com.mreshet.mrComps.mainWindow.ext.hamMainWindow.HamMainWindow;
	import com.mreshet.mrComps.popup.PopupWarning;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class AppMainWindow extends HamMainWindow
	{
		//private var _tabBar: 				FlexTabBar 		= null;
		//private var _actionBar:			ActionBar 		= null;
		private var _quadStrip2:		Quad					=	null;
		private var _quadStrip1:		Quad					=	null;

		private var _ppLeaving:			PopupWarning 	= null;
		///=private var _ppFacebook:		PopupWarning 	= null;
		
		private var _appController:	AppController = null;
		
		//private var _btnShare:			Button				=	null;
		//private var _btnPrev:				Button				=	null;

		public function AppMainWindow()
		{
			super();

			hamProperties.animateOpenPopUpTime 	= 0.3;
			hamProperties.animateClosePopUpTime = 0.2;
			
			_appController 											= AppController.instance;
		}
		
		override protected function initialize():void
		{
			super.initialize();
			
			_quadStrip1													= new Quad(1, 1, 0x748691);
			_quadStrip2													= new Quad(1, 1, 0xf0f0f0);
			//_tabBar													= new FlexTabBar();
			//_actionBar 												= new ActionBar();
			
			//_btnShare														=	CompsFactory.newButton("general::ss1.btn_share_app","general::ss1.btn_share_app");
			//_btnPrev														=	CompsFactory.newButton("general::ss1.btn_back","general::ss1.btn_back");
			
			/*var abdp:	Vector.<Object> 					= Vector.<Object>([
				{ id: "logo", 			src: CompsFactory.newImage("general::ss1.logo_main"), 																				align: "center"},
				{ id: "left", 			src: _btnShare, 																																							align: "left"},
				{ id: "profile", 		src: CompsFactory.newButton("general::ss1.btn_profile_up", "general::ss1.btn_profile_down"), 	align: "right"},
			]);*/

			/*_actionBar.dataProvider  						= abdp;
			_actionBar.percentHeight 						= 9;
			_actionBar.leftItemsPercentWidth 		= 11;
			_actionBar.leftItemsPercentHeight		= 40;
			_actionBar.rightItemsPercentWidth 	= 11;
			_actionBar.rightItemsPercentHeight 	= 60;
			_actionBar.centerItemsPercentWidth 	= 50;
			_actionBar.centerItemsPercentHeight	= 70;
			_actionBar.paddingLeft 							= 8;
			_actionBar.paddingRight 						= 8;
			_actionBar.bgSkin 									= 0xEF7F3C;
			//_actionBar.onSelected 							= actionBar_onTriggered;

			//
			_tabBar.bgSkin 											= 0xEF7F3C;
			_tabBar.percentHeight								=	11;
			_tabBar.paddingLeft 								= _tabBar.paddingRight = 20;
			_tabBar.gap  												= 10*/
				
			/*var tbdp:	Vector.<Object> 					= Vector.<Object>([
				{ id: "liked", 		textureDown: "general::ss1.btn_like2_down", 	textureUp: "general::ss1.btn_like2_up"},
				{ id: "recom", 		textureDown: "general::ss1.btn_star_down", 		textureUp: "general::ss1.btn_star_up"},
				{ id: "write", 		textureDown: "general::ss1.btn_pencil_down", 	textureUp: "general::ss1.btn_pencil_up"},
				{ id: "fame", 		textureDown: "general::ss1.btn_fame_down", 		textureUp: "general::ss1.btn_fame_up"},
				{ id: "home", 		textureDown: "general::ss1.btn_home_down", 		textureUp: "general::ss1.btn_home_up"},
			]);*/
				
			//_tabBar.onSelected									=	tabBar_onTriggered;
			//_tabBar.dataProvider 								= tbdp;
			
			_ppLeaving													=	new PopupWarning();
			_ppLeaving.onAction									=	popup_onAction;
			_ppLeaving.visible 									= false;
			_ppLeaving.textNo										=	"אל";
			_ppLeaving.textYes									=	"ןכ";
			_ppLeaving.textWarning							=	"?תאצל";
			
			/*_ppFacebook													=	new PopupWarning();
			_ppFacebook.onAction								=	popup_onAction;
			_ppFacebook.visible 								= false;
			_ppFacebook.textYes									=	"רושיא";
			_ppFacebook.textWarning							=	TextUtils.detectHebrew("האפליקציה מחייבת גישה לפייסבוק");
			_ppFacebook.textHeadline						=	TextUtils.detectHebrew("מצטערים");*/
			
			//addChild(_tabBar);
			//addChild(_actionBar);
			addChild(_quadStrip1);
			addChild(_ppLeaving);
			//addChild(_ppFacebook);
		}
		
		private function popup_onAction(action: String):void
		{
			switch(action)
			{
				case PopupWarning.YES:
				{
					_appController.exitApplication();
					break;
				}
				case PopupWarning.NO:
				{
					_ppLeaving.visible = false;
					break;
				}
			}
			
		}
		
		override protected function draw():void
		{
			if(isInvalid(INVALIDATION_FLAG_SIZE)) {
				//hamProperties.animateToX 														= -width*0.7;
				//hamProperties.animateFromX 													= 0;
				invalidate(INVALIDATION_FLAG_HAM_PROPERTIES);
			}
			
			/*_tabBar.width								 													= width;
			_tabBar.validate();
			_tabBar.y 																						= height - _tabBar.height;
			
			_actionBar.width 																			= width;
			_actionBar.y 																					= 0;*/
			//_actionBar.validate();
			
			_quadStrip1.width																			=	width;
			_quadStrip1.height																		=	height*0.001;
			//_quadStrip1.y																					=	_actionBar.y + _actionBar.height;
			
			_quadStrip2.width																			=	width;
			_quadStrip2.height																		=	0;
			_quadStrip2.y																					=	_quadStrip1.y + _quadStrip1.height;
			
			_navigator.width																			= width;
			//_navigator.height																			=	height - (_actionBar.height + _tabBar.height + _quadStrip1.height + _quadStrip2.height);
			_navigator.y																					=	_quadStrip2.y + _quadStrip2.height;
						
			_ppLeaving.width																			=	width;
			_ppLeaving.height																			=	height;
			
			super.draw();
		}
				
		public function showWarning():void
		{
			_ppLeaving.visible = true;
		}
		
		private function tabBar_onTriggered($id:String):void
		{
      var cfid: String = "";
      
			switch($id)
			{
				case "home":
				{
					break;
				}
				case "fame":
				{
					break;
				}
				case "write":
				{
					break;
				}
				case "liked":
				{
          break;
				}
				case "recom":
				{
					break;
				}
					
				default:
				{
					break;
				}
			}
			
		}			

		override public function navigator_onTransitionComplete(event:Event):void
		{
			trace(_navigator.activeScreenID);
			
			_appController.removeSplash();

			switch(_navigator.activeScreenID)
			{
				case "sView_HomeScreen":
				{
					toggleTabBarVisibility(false);
					
					//_actionBar.mrUpdateContent("left", _btnShare);
					
					//_actionBar.mrUpdateContent("logo", CompsFactory.newImage("general::ss1.logo_main"));
					_appController.gaTrackPageview("view_HomeScreen");
					break;
				}
					
				default:
				{
					break;
				}
			}
		}

		public function toggleTabBarVisibility($visible:Boolean):void
		{
			//_tabBar.visible = $visible;
			return;
			//if(_tabBar.visible) {
			//	_navigator.height		=	height - (_actionBar.height + _tabBar.height + _quadStrip1.height + _quadStrip2.height);
			//}
			//else
			//	_navigator.height		=	height - (_actionBar.height + 0 + _quadStrip1.height + _quadStrip2.height);
				
		}

	}
}