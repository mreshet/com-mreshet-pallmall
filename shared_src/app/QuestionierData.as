package app
{
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SQLEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.globalization.DateTimeFormatter;
	import flash.globalization.DateTimeStyle;
	import flash.globalization.LocaleID;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class QuestionierData
	{
		private static var _instance:			QuestionierData					=	null;
	
		private var _data_host:  String = "";  // Name of host
		private var _data_city:  String = "";  // Name of city
		private var _data_date:  String = "";  // Current date
		private var _data_sex:   String = "";  // Gender
		private var _data_smoke: String = "";  // Smoking brand
		private var _data_code:  String = "";  // Code
		private var _data_email: String = "";  // Email
		private var _data_prefer:String = "";  //Male/Female
		private var _data_myAge: String = "";  //Age
		private var _data_smooth:String = ""; //Results
		private var _data_agree:String = ""; //Results
		
		
		private var _currScores:uint = 0; //Results
		private var _connection:SQLConnection = null;
		
		public function getHost():String
		{
			return _data_host;
		}
		
		public function getCity():String
		{
			return _data_city;
		}
		
		public function clearParticipant():void
		{
			_data_sex    = ""; // Gender
			_data_smoke  = ""; // Smoking brand
			_data_code   = ""; // Code
			_data_email  = ""; // Email
			_data_prefer = ""; //Male/Female
			_data_myAge  = ""; //Age
			//_data_smooth = ""; //Results
			_data_agree =  "";
			_currScores = 0;
		}
		
		public function getScore():uint
		{
			return _currScores;
		}
		public function addScores(score:uint):void
		{
			_currScores += score;
			trace("_currScores = " + _currScores);
		}
		public function QuestionierData()
		{
			if(_instance	!=	null)
				throw new Error("AppController is singleton!");
			
			/** First instanciation */
			_instance					=	this;
		}
		
		public static function get instance():QuestionierData
		{
			return ((_instance) ? _instance : new QuestionierData());
		}
		
		public function addRecord():void 
		{
			var url:String = "http://cigarette.mreshet.co.il/api/tablet";
			var request:URLRequest = new URLRequest(url);
			var requestVars:URLVariables = new URLVariables();
			requestVars.data_hash = "bgd2#$%sg51-123a**+*-44asdfvbn3355";
			requestVars.data_host = _data_host;
			requestVars.data_city = _data_city;
			
			var df:DateTimeFormatter = new DateTimeFormatter(LocaleID.DEFAULT, DateTimeStyle.SHORT, DateTimeStyle.NONE);
			var currentDate:Date = new Date();
			var shortDate:String = df.format(currentDate);
			_data_date = shortDate;
			
			requestVars.data_date = _data_date; 
			
			requestVars.data_sex = _data_sex;
			requestVars.data_smoke = _data_smoke;
			requestVars.data_code = _data_code;
			requestVars.data_email = _data_email;
			requestVars.data_prefer = _data_prefer;
			requestVars.data_myAge = _data_myAge;
			requestVars.data_agree = _data_agree;
			
			_data_smooth = _currScores.toString()
			requestVars.data_smooth = _data_smooth;
			
			request.data = requestVars;
			request.method = URLRequestMethod.POST;
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, loaderCompleteHandler, false, 0, true);
			urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler, false, 0, true);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler, false, 0, true);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler, false, 0, true);
			for (var prop:String in requestVars) {
				trace("Sent: " + prop + " is: " + requestVars[prop]);
			}
			try {
				urlLoader.load(request);
			} catch (e:Error) {
				trace(e);
			}
			
			saveToDatabase();
		}
		
		private function loaderCompleteHandler(e:Event):void 
		{
			//var responseVars:URLVariables = URLVariables( e.target.data );
			//trace( "responseVars: " + responseVars );
			
		}
		private function httpStatusHandler( e:HTTPStatusEvent ):void 
		{
			trace("httpStatusHandler:" + e);
		}
		
		private function securityErrorHandler( e:SecurityErrorEvent ):void {
			trace("securityErrorHandler:" + e);
		}
		private function ioErrorHandler( e:IOErrorEvent ):void {
			trace("ORNLoader:ioErrorHandler: " + e);
			//dispatchEvent( e );
		}
		
		public function updateAttendant(name:String, city:String):void
		{
			_data_host = name;
			_data_city = city;
			
		}
		
		public function updateParticipantDetails(age:String, 
											    gender:String, 
											    brand:String, 
											    code:String, 
											    email:String,
												isAgree:String):void
		{
			_data_sex    = gender;
			_data_smoke  = brand;
			_data_code   = code;
			_data_email  = email;
			_data_myAge  = age;
			_data_agree = isAgree;
		}
		
		public function getSex():String
		{
			return _data_sex;
		}
		
		public function getPrefer():String
		{
			return _data_prefer;
		}
		
		public function updateParticipantGenger(prefer:String):void
		{
			_data_prefer = prefer;
		}
		
		private function saveToDatabase():void
		{
			_connection = new SQLConnection(); 
			
			// The database file is in the application storage directory 
			var folder:File = File.applicationStorageDirectory; 
			var dbFile:File = folder.resolvePath("ContactsPallMall.db"); 
			
			try 
			{ 
				_connection.open(dbFile); 
				trace("the database was created successfully"); 
			} 
			catch (error) 
			{ 
				trace("Error message:", error.message); 
				trace("Details:", error.details); 
			}
			
			var myTableCreate:String = "";
			myTableCreate += "CREATE TABLE IF NOT EXISTS contacts (";
			myTableCreate += "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ";
			myTableCreate += "data_created DATE DEFAULT (datetime('now','localtime')), "; 
			myTableCreate += "data_host TEXT, "; //1
			myTableCreate += "data_city TEXT, "; //2
			myTableCreate += "data_date TEXT, "; //3
			myTableCreate += "data_sex TEXT, ";  //4
			myTableCreate += "data_smoke TEXT, ";  //5
			myTableCreate += "data_code TEXT, ";  //6
			myTableCreate += "data_email TEXT, ";  //7
			myTableCreate += "data_prefer TEXT, ";  //8
			myTableCreate += "data_myAge TEXT, ";  //9
			myTableCreate += "data_smooth TEXT,";  //10
			myTableCreate += "data_agree TEXT";  //11
			myTableCreate += ")";
			
			var createStmt:SQLStatement = new SQLStatement(); 
			createStmt.sqlConnection = _connection; 
			createStmt.text = myTableCreate; 
			try 
			{ 
				createStmt.execute(); 
				trace("Table created"); 
			} 
			catch (error) 
			{ 
				trace("Error message:", error.message); 
				trace("Details:", error.details); 
			}
			createNew()
		}
		
		private function createNew():void
		{
			var stat:SQLStatement = new SQLStatement();
			stat.sqlConnection = _connection;
			stat.text = "INSERT INTO contacts (data_host, data_city, data_date, data_sex, data_smoke, data_code, data_email, data_prefer, data_myAge, data_smooth, data_agree) VALUES (@data_host, @data_city, @data_date, @data_sex, @data_smoke, @data_code, @data_email, @data_prefer, @data_myAge, @data_smooth, @data_agree)";
			
			//stat.parameters["@data_created"] = new Date();
			stat.parameters["@data_host"] = _data_host;   //1
			stat.parameters["@data_city"] = _data_city;   //2
			stat.parameters["@data_date"] = _data_date;  //3
			stat.parameters["@data_sex"]  = _data_sex;   //4
			stat.parameters["@data_smoke"] = _data_smoke;//5
			stat.parameters["@data_code"] = _data_code;  //6
			stat.parameters["@data_email"] = _data_email; //7
			stat.parameters["@data_prefer"] = _data_prefer;//8
			stat.parameters["@data_myAge"] = _data_myAge; //9
			stat.parameters["@data_smooth"] = _data_smoke;//10
			stat.parameters["@data_agree"] = _data_agree;//11
			try 
			{ 
				stat.execute(); 
				trace("Row Added"); 
			} 
			catch (error) 
			{ 
				trace("Error message:", error.message); 
				trace("Details:", error.details); 
			}
			_connection.close();
		}
		
		private function onSave(e:SQLEvent):void
		{
			trace("onSave = " + e.toString());
		}		
	}
}