package app
{		
	import com.mreshet.mrComps.mrScreenNavigator.MrScreenNavigator;
	
	import events.ControllerEvent;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import views.View_AttendantScreen;
	import views.View_CalcScreen;
	import views.View_EndScreen;
	import views.View_GameScreen;
	import views.View_MaleFemaleScreen;
	import views.View_ParticipantScreen;
	import views.View_ResultsScreen;
	import views.View_WelcomeScreen;

	/**
	 * TODO: design an interface
	 * Main Starling sprite controller,
	 * this is the base Starling sprite
	 * 
	 */
	public class AppMainStarling extends Sprite
	{
		private var _mainWindow:						AppMainWindow													= null;
		
		private var _appController: 				AppController 												= null;
				
		public function AppMainStarling()
		{
			super();
			
			_appController	=	AppController.instance;
						
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}
		
		private function onScreenRotate(event:Event):void
		{
			AppGlobals.stageWidth	  					=	stage.stageWidth;
			AppGlobals.stageHeight	  				=	stage.stageHeight;
		
			_mainWindow.width 								= stage.stageWidth;
			_mainWindow.height 								= stage.stageHeight;
		}
		
		protected function addedToStageHandler(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);

			stage.addEventListener(Event.RESIZE, onScreenRotate);
			
			AppGlobals.stageWidth	          	=	stage.stageWidth;
			AppGlobals.stageHeight	          =	stage.stageHeight;

			/**
			 * 
			 */

			_mainWindow 											= new AppMainWindow();
			_mainWindow.addEventListener(Event.ADDED_TO_STAGE, onMainWindowAddedToStage);
									
			_appController.addEventListener(ControllerEvent.READY, onControllerready);
			_appController.contentStart(this);
		}
		
		private function onMainWindowAddedToStage(event:Event):void
		{
			_mainWindow.removeEventListener(Event.ADDED_TO_STAGE, onMainWindowAddedToStage);
			
//			/var v1:View_HomeScreen	=	new View_HomeScreen();
			
			_mainWindow.navigator.addScreenWithDetails(View_AttendantScreen,	'sView_AttendantScreen',	{}, null);
			_mainWindow.navigator.addScreenWithDetails(View_ParticipantScreen,	'sView_ParticipantScreen',	{}, null);
			_mainWindow.navigator.addScreenWithDetails(View_MaleFemaleScreen,	'sView_MaleFemaleScreen',	{}, null);
			_mainWindow.navigator.addScreenWithDetails(View_WelcomeScreen,		'sView_WelcomeScreen',	   	{}, null);
			_mainWindow.navigator.addScreenWithDetails(View_GameScreen,			'sView_GameScreen',	      	{}, null);
			_mainWindow.navigator.addScreenWithDetails(View_CalcScreen,			'sView_CalcScreen',	      	{}, null);
			_mainWindow.navigator.addScreenWithDetails(View_ResultsScreen,		'sView_ResultsScreen',	      	{}, null);
			_mainWindow.navigator.addScreenWithDetails(View_EndScreen,			'sView_EndScreen',	      	{}, null);
			
			_mainWindow.width 		= stage.stageWidth;
			_mainWindow.height 		= stage.stageHeight;
			
			_mainWindow.navigator.pushScreen("sView_AttendantScreen");
		}
		
		protected function onControllerready(event:ControllerEvent):void
		{
			trace("_roController is Ready!!!")
			
			addChildAt(_mainWindow, 0)
		}
		
		public function showScreen($id: String): void
		{
			_mainWindow.navigator.showScreen($id);
		}
		

		public function get navigator(): MrScreenNavigator {
			return _mainWindow.navigator;
		}

		public function get mainWindow():AppMainWindow
		{
			return _mainWindow;
		}

	}
}