package
{
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.events.UncaughtErrorEvent;
	import flash.geom.Rectangle;
	import flash.utils.getQualifiedClassName;
	
	import app.AppController;
	import app.AppMainStarling;
	
	import assets.SEmbeddedFonts;
		
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.ITextRenderer;
	
	import starling.core.Starling;
	import starling.events.Event;

	[SWF(frameRate="60")]
	public class Main extends Sprite
	{
		private var _starling:			Starling 			= null;
		private var _appController:	AppController 			= null;
		
		public function Main()
		{
			_appController 	= AppController.instance;
			addEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);
			mouseEnabled 		= mouseChildren = false;
		}
		
		protected function onAddedToStage(event:flash.events.Event):void
		{
			removeEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);

			if (stage)
			{
				stage.color 				= 0xf0f0f0;
				stage.scaleMode 		= StageScaleMode.NO_SCALE;
				stage.align 				= StageAlign.TOP_LEFT;
				stage.mouseChildren = false;
				//stage.quality				=	StageQuality.BEST;
			}
			
			SEmbeddedFonts.registerFonts();
			
			_appController.startSplash(this);

			loaderInfo.addEventListener(flash.events.Event.COMPLETE, loaderInfo_completeHandler);
		}
		
		private function loaderInfo_completeHandler(event: flash.events.Event):void
		{
			loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, onUncaughtError);
			
			Starling.handleLostContext 		= true;
			Starling.multitouchEnabled 		= true;
			
			FeathersControl.defaultTextRendererFactory = function(): ITextRenderer
			{
				return new TextFieldTextRenderer();
			};
			
			_starling 										= new Starling(AppMainStarling, stage, null, null, Context3DRenderMode.AUTO, Context3DProfile.BASELINE);
			_starling.enableErrorChecking = false;
			_starling.antiAliasing 				= 4;
			_starling.addEventListener(starling.events.Event.ROOT_CREATED, starling_onRootCreated);
			_starling.start();
			//_starling.showStats 					= true;
			
			stage.addEventListener(flash.events.Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
			stage.addEventListener(flash.events.Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
		}
		
		private function onUncaughtError(event: UncaughtErrorEvent): void
		{
			trace("uncaught error: " + event.error);
			NativeApplication.nativeApplication.exit();
		}
		
		private function starling_onRootCreated():void
		{
			_starling.removeEventListener(starling.events.Event.ROOT_CREATED, starling_onRootCreated);
		}
		
		private function stage_resizeHandler(event: flash.events.Event): void
		{
			_starling.stage.stageWidth 	= stage.stageWidth;
			_starling.stage.stageHeight = stage.stageHeight;
			
			var viewPort: Rectangle 		= _starling.viewPort;
			viewPort.width 					= stage.stageWidth;
			viewPort.height 				= stage.stageHeight;
			
			try	{
				_starling.viewPort 				= viewPort;
			}
			catch (error: Error) {
				trace("stage_resizeHandler: " + event);
			}
		}
		
		private function stage_deactivateHandler(event: flash.events.Event): void
		{
			_starling.stop();
			stage.addEventListener(flash.events.Event.ACTIVATE, stage_activateHandler, false, 0, true);
		}
		
		private function stage_activateHandler(event: flash.events.Event): void
		{
			stage.removeEventListener(flash.events.Event.ACTIVATE, stage_activateHandler);
			_starling.start();
		}
	}
}